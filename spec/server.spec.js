let proxyquire = require('proxyquire');
let td = require('testdouble');
const DatabaseCommunicator = require('../src/database-communicator/database-communicator');
const WebSocket = require('ws');
const SocketMap = require('../src/helpers/socket-map');

let mockApp;
let mockCors, mockCorsCaptor;
let mockCorsReturnValue;
let mockDatabaseCommunicator, mockDbCommunicatorReadyPromise, readyDatabaseCommunicator;
let originalPORT = process.env.PORT;

let heisldiceRouter;
let mockHttpServer;
let MockWebsocketModule, mockWebsocket, websocketRequest;
let mockWebsocketServer;
let mockWebsocketSetup;
let gameID;

const setup = (shouldSetUpReadyPromiseToCallBack) => {
  process.env.PORT = 9942

  mockApp = td.object(['use', 'get', 'post', 'listen']);

  const mockDatabaseCommunicatorClass = td.constructor(DatabaseCommunicator);
  mockDatabaseCommunicator = new mockDatabaseCommunicatorClass();
  readyDatabaseCommunicator = { name: "Ready Database Communicator" };
  mockDbCommunicatorReadyPromise = td.object(['then']);
  td.when(mockDatabaseCommunicator.whenReady()).thenReturn(mockDbCommunicatorReadyPromise);
  if (shouldSetUpReadyPromiseToCallBack) {
    td.when(mockDbCommunicatorReadyPromise.then()).thenCallback(readyDatabaseCommunicator);
  }

  mockCors = td.function('mock CORS');
  mockCorsCaptor = td.matchers.captor();
  mockCorsReturnValue = { name: 'I am the return value from call to cors' };
  td.when(mockCors(mockCorsCaptor.capture())).thenReturn(mockCorsReturnValue)

  mockBodyParserMiddleware = td.function('bodyParser');
  const mockBodyParserModule = {
    json: (options) => {
      return mockBodyParserMiddleware;
    }
  };

  MockWebsocketModule = { Server: td.constructor(WebSocket.Server) }
  mockWebsocketSetup = td.function('websocket server setup');
  mockWebsocketServer = { web: 'socket' };

  mockWebsocket = td.object(['on', 'close']);
  gameID = "RTEW";
  websocketRequest = { url: `ws://localhost:8000?gameID=${gameID}` };

  heisldiceRouter = { router: 'for heisldice' };
  const mockHeisldiceRoutes = td.object(['getRouter']);
  td.when(mockHeisldiceRoutes.getRouter(readyDatabaseCommunicator)).thenReturn(heisldiceRouter);
  
  dominoesRouter = { router: 'for dominoes' };
  const mockDominoesRoutes = td.object(['getRouter']);
  td.when(mockDominoesRoutes.getRouter(readyDatabaseCommunicator)).thenReturn(dominoesRouter);

  mockHttpServer = td.object(['listen']);
  const mockHttp = td.object(['createServer']);
  td.when(mockHttp.createServer(mockApp)).thenReturn(mockHttpServer);
  td.when(MockWebsocketModule.Server({ server: mockHttpServer })).thenReturn(mockWebsocketServer);

  proxyquire('../server.js', {
    express: () => {
      return mockApp;
    },
    cors: mockCors,
    './configuration.js': { getCorsWhitelist: () => { return ['ryan'] }, getPort: () => { return 1234 } },
    'body-parser': mockBodyParserModule,
    './src/database-communicator/database-communicator': mockDatabaseCommunicatorClass,
    './src/routers/heisldice-routes': mockHeisldiceRoutes,
    './src/routers/dominoes-routes': mockDominoesRoutes,
    'ws': MockWebsocketModule,
    './src/helpers/websocket-server-setup': mockWebsocketSetup,
    'http': mockHttp,
  });
}

describe('server', () => {

  beforeEach(() => {
    setup(true);
  });

  afterEach(() => {
    SocketMap.instance = null
  });

  it('should set cors properly', () => {
    const originProvider = mockCorsCaptor.value.origin;

    const corsOriginCallback_Success = td.function();
    originProvider('ryan', corsOriginCallback_Success);

    td.verify(corsOriginCallback_Success(null, true));

    const corsOriginCallback_Failure = td.function();
    originProvider('billy', corsOriginCallback_Failure);

    td.verify(corsOriginCallback_Failure(new Error('billy not allowed by CORS')));

    td.verify(mockApp.use(mockCorsReturnValue));
  });

  it('should use body parser middleware to parse JSON in requests', () => {
    td.verify(mockApp.use(mockBodyParserMiddleware));
  });

  it('should make a Websocket server and pass it to setup', () => {
    td.verify(mockWebsocketSetup(mockWebsocketServer));
  });

  describe('endpoints', () => {
    it('should get and use a router for the heisldice endpoints', () => {
      td.verify(mockApp.use('/heisldice', heisldiceRouter));
    });

    it('should get and use a router for the dominoes endpoints', () => {
      td.verify(mockApp.use('/dominoes', dominoesRouter));
    });

    it('should contain a default endpoint', () => {
      const notFoundFunctionCaptor = td.matchers.captor();
      td.verify(mockApp.get('/*', notFoundFunctionCaptor.capture()));

      const mockResponse = td.object(['status']);
      const mockResponseWithStatus = td.object(['send']);
      td.when(mockResponse.status(404)).thenReturn(mockResponseWithStatus);

      notFoundFunctionCaptor.value(null, mockResponse);
      td.verify(mockResponseWithStatus.send());
    });
  });
});

describe('server - listen', () => {
  beforeEach(() => {
    setup(false);
  });

  afterEach(() => {
    process.env.PORT = originalPORT;
  });

  it('should listen on port from environment', () => {
    const readyCaptor = td.matchers.captor();

    td.verify(mockDbCommunicatorReadyPromise.then(readyCaptor.capture()));
    td.verify(mockHttpServer.listen(td.matchers.anything()), { times: 0 });

    readyCaptor.value();

    td.verify(mockHttpServer.listen('9942'));
  });
});
