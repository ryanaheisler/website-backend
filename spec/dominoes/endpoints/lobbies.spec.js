const proxyquire = require('proxyquire');
const td = require('testdouble');

describe('Lobbies Endpoint', () => {

    let endpoint;
    let mockLogger;
    let request, response, responseWithStatus;
    let databaseCommunicator;
    let mockStartLobby;

    beforeEach(() => {
        mockLogger = td.object(['error']);

        request = {
            body: {},
            params: {}
        };

        response = td.object(['status']);
        responseWithStatus = td.object(['send', 'end']);

        databaseCommunicator = { "I'm": "a communicumber" };

        mockStartLobby = td.object(['execute']);

        endpoint = proxyquire('../../../src/dominoes/endpoints/lobbies.js', {
            '../handlers/start-lobby-handler': mockStartLobby,
            '../../logger/logger': mockLogger
        });
    });

    describe('POST', () => {
        it('should respond with 201 status', (done) => {
            const lobbyID = 'K90A'
            td.when(mockStartLobby.execute(databaseCommunicator)).thenResolve(lobbyID);
            td.when(response.status(201)).thenReturn(responseWithStatus);

            const middleware = endpoint.POST(databaseCommunicator);
            middleware(request, response);

            setTimeout(() => {
                td.verify(responseWithStatus.send({ lobbyID: lobbyID }));
                done();
            });
        });

        it('should return a 500 status', (done) => {
            const error = new Error('mmmmmmmmm');
            td.when(mockStartLobby.execute(databaseCommunicator)).thenReject(error);
            td.when(response.status(500)).thenReturn(responseWithStatus);

            const middleware = endpoint.POST(databaseCommunicator);
            middleware(request, response);

            setTimeout(() => {
                td.verify(mockLogger.error(error));
                td.verify(responseWithStatus.send({ error: error.message }));
                done();
            });
        });
    });

    describe('GET', () => {
        beforeEach(() => {
            request = {
                body: {},
                lobby: {
                    lobbyID: "893A"
                },
                params: {
                    lobbyID: "893A",
                }
            };
        });
        it('should respond with lobby and 200 status', (done) => {
            td.when(response.status(200)).thenReturn(responseWithStatus);

            const middleware = endpoint.GET(databaseCommunicator);
            middleware(request, response);

            setTimeout(() => {
                td.verify(responseWithStatus.send({ lobby: request.lobby }));
                done();
            });
        });
    });
});