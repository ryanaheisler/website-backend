const td = require('testdouble');
const proxyquire = require('proxyquire');

describe('DominoesLobbyFactory', () => {

  const lobbyID = "K29S";

  it('should build a new lobby', () => {
    const mockGenerator = td.object(['getID']);
    td.when(mockGenerator.getID()).thenReturn(lobbyID);

    const factory = proxyquire("../../../src/dominoes/factories/dominoes-lobby-factory", {
      '../../helpers/game-id-generator': mockGenerator
    });

    const lobby = factory.build();
    expect(lobby.players).toEqual([]);
    expect(lobby.lobbyID).toEqual(lobbyID);
    expect(lobby.startTime).toBeGreaterThanOrEqual(Date.now() - 10);
    expect(lobby.startTime).toBeLessThan(Date.now() + 100);
  });
});