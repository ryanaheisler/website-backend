const td = require('testdouble');
const proxyquire = require('proxyquire');

describe('StartLobbyHandler', () => {
  let handler;
  let mockDatabaseCommunicator;
  let mockLobbyFactory;
  let lobby;

  beforeEach(() => {
    lobby = {
      players: [],
      lobbyID: "1234"
    };

    mockDatabaseCommunicator = td.object(['insertDominoesLobby', 'getCurrentDominoesLobby']);

    mockLobbyFactory = td.object(['build']);
    td.when(mockLobbyFactory.build()).thenReturn(lobby);

    handler = proxyquire("../../../src/dominoes/handlers/start-lobby-handler", {
      "../factories/dominoes-lobby-factory": mockLobbyFactory
    });
  });

  it('should create a new lobby with an ID and resolve that lobby\'s ID', () => {
    td.when(mockDatabaseCommunicator.insertDominoesLobby(lobby)).thenResolve();
    return handler.execute(mockDatabaseCommunicator).then((result) => {
      expect(result).toEqual(lobby.lobbyID);
    });
  });

  it('should create new lobby until the lobby ID is unique among active lobbies', async () => {
    const uniqueLobby = {
      players: [],
      lobbyID: "9999"
    };
    const existingLobby = {
      players: ["Ryan", "Alex", "Matt"],
      lobbyID: "1234"
    };
    td.when(mockLobbyFactory.build()).thenReturn(lobby, lobby, uniqueLobby);
    td.when(mockDatabaseCommunicator.getCurrentDominoesLobby(lobby.lobbyID)).thenResolve(existingLobby);
    td.when(mockDatabaseCommunicator.getCurrentDominoesLobby(uniqueLobby.lobbyID)).thenResolve(undefined);
    td.when(mockDatabaseCommunicator.insertDominoesLobby(uniqueLobby)).thenResolve();

    const result = await handler.execute(mockDatabaseCommunicator);

    expect(result).toEqual(uniqueLobby.lobbyID);
  });

  describe('should bubble up promise rejection', () => {
    it('from creating a new lobby', () => {
      const errorToThrow = new Error('hello');
      td.when(mockDatabaseCommunicator.insertDominoesLobby(lobby)).thenReject(errorToThrow);

      return handler.execute(mockDatabaseCommunicator).then((result) => {
        fail('returned promise did not reject');
      }).catch((error) => {
        expect(error).toEqual(errorToThrow);
      });
    });
  });
});