const LobbyExtractor = require('../../../src/dominoes/middleware/lobby-extractor');
const td = require('testdouble');
const constants = require('../../../src/dominoes/constants');

describe('LobbyExtractor', () => {

  let request;
  let response;
  let responseWithStatus;
  let next;
  let databaseCommunicator;
  let middleware;
  let lobby;

  beforeEach(() => {
    request = {
      params: {
        lobbyID: "WERT"
      }
    }
    response = td.object(['status']);
    responseWithStatus = td.object(['send']);
    next = td.function('next');

    databaseCommunicator = td.object(['getCurrentDominoesLobby']);
    middleware = LobbyExtractor(databaseCommunicator);
  });

  it('should get lobby and add it to the request', (done) => {
    lobby = {
      lobbyID: request.params.lobbyID
    }

    td.when(databaseCommunicator.getCurrentDominoesLobby(request.params.lobbyID)).thenResolve(lobby);

    middleware(request, response, next);

    setTimeout(() => {
      td.verify(next());
      expect(request.lobby).toBe(lobby);
      done();
    }, 100);
  });

  it('should respond with error code if there is no current lobby', (done) => {
    const errorMessage = constants.ERROR_MESSAGES.LOBBY_NOT_FOUND(request.params.lobbyID);

    td.when(databaseCommunicator.getCurrentDominoesLobby(request.params.lobbyID)).thenResolve(undefined);
    td.when(response.status(404)).thenReturn(responseWithStatus);

    middleware(request, response, next);

    setTimeout(() => {
      td.verify(responseWithStatus.send({ error: errorMessage }));
      done();
    }, 100);
  });
});