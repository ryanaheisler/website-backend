let td = require('testdouble');
let proxyquire = require('proxyquire');

const SocketMap = require('../..//src/helpers/socket-map');
const pruneAndPing = require('../..//src/helpers/prune-websockets');

describe('Prune Websockets', () => {
    let gameID1_2, gameID3_4;
    let liveConnection1, liveConnection2, liveConnection3, liveConnection4;
    let deadConnection1, deadConnection2, deadConnection3, deadConnection4;

    let socketMap;

    beforeEach(() => {
        gameID1_2 = "PEKA"
        gameID3_4 = "OWEJ"

        liveConnection1 = td.object(['terminate', 'ping']);
        liveConnection2 = td.object(['terminate', 'ping']);
        liveConnection3 = td.object(['terminate', 'ping']);
        liveConnection4 = td.object(['terminate', 'ping']);
        liveConnection1.isAlive = true
        liveConnection2.isAlive = true
        liveConnection3.isAlive = true
        liveConnection4.isAlive = true

        deadConnection1 = td.object(['terminate', 'ping']);
        deadConnection2 = td.object(['terminate', 'ping']);
        deadConnection3 = td.object(['terminate', 'ping']);
        deadConnection4 = td.object(['terminate', 'ping']);
        deadConnection1.isAlive = false
        deadConnection2.isAlive = false
        deadConnection3.isAlive = false
        deadConnection4.isAlive = false

        socketMap = SocketMap.SINGLETON();

        socketMap.add(liveConnection1, gameID1_2)
        socketMap.add(liveConnection2, gameID1_2)
        socketMap.add(liveConnection3, gameID3_4)
        socketMap.add(liveConnection4, gameID3_4)

        socketMap.add(deadConnection1, gameID1_2)
        socketMap.add(deadConnection2, gameID1_2)
        socketMap.add(deadConnection3, gameID3_4)
        socketMap.add(deadConnection4, gameID3_4)
    });

    afterEach(() => {
        SocketMap.instance = null;
    });

    it('should crawl the SocketMap, terminating and pruning dead connections and pinging live ones', () => {
        pruneAndPing();

        td.verify(liveConnection1.ping(td.matchers.anything()));
        expect(liveConnection1.isAlive).toBeFalse();
        td.verify(liveConnection2.ping(td.matchers.anything()));
        expect(liveConnection2.isAlive).toBeFalse();
        td.verify(liveConnection3.ping(td.matchers.anything()));
        expect(liveConnection3.isAlive).toBeFalse();
        td.verify(liveConnection4.ping(td.matchers.anything()));
        expect(liveConnection4.isAlive).toBeFalse();

        td.verify(deadConnection1.terminate());
        td.verify(deadConnection2.terminate());
        td.verify(deadConnection3.terminate());
        td.verify(deadConnection4.terminate());

        const clientsForGame1 = socketMap.getClients(gameID1_2);
        expect(clientsForGame1).toEqual([liveConnection1, liveConnection2])
        const clientsForGame2 = socketMap.getClients(gameID3_4);
        expect(clientsForGame2).toEqual([liveConnection3, liveConnection4])
    });
});