const SocketMap = require('../../src/helpers/socket-map');

describe('SocketMap', () => {

  afterEach(() => {
    SocketMap.instance = null;
  });

  it('should be a singleton', () => {
    const map1 = SocketMap.SINGLETON();
    const map2 = SocketMap.SINGLETON();

    expect(map1).not.toBeNull();
    expect(map2).not.toBeNull();

    expect(map1).toBe(map2);

    expect(map1 instanceof SocketMap);
  });

  it('should add a socket to the map if the ID is new to the map', () => {
    const map = SocketMap.SINGLETON();
    const ID = "PPOR";

    expect(map.getClients(ID)).toEqual([]);

    const socket = { socket: 'socket' };
    map.add(socket, ID);

    expect(map.socketMap[ID].length).toEqual(1);
    expect(map.socketMap[ID][0]).toBe(socket);
  });

  it('should add a socket to the map if there are already sockets for that group', () => {
    const map = SocketMap.SINGLETON();
    const ID = "WPOD";
    const existing = { existing: 'socket' };
    map.socketMap[ID] = [existing];

    const socket = { new: 'socket' };
    map.add(socket, ID);

    expect(map.socketMap[ID].length).toEqual(2);
    expect(map.socketMap[ID][0]).toBe(existing, socket);
  });

  it('should remove an array of sockets', () => {
    const map = SocketMap.SINGLETON();
    const ID = "OPSD";
    const existing1 = { existing: 'socket1' };
    const existing2 = { existing: 'socket2' };
    map.socketMap[ID] = [existing1, existing2];

    map.removeGroup(ID);

    expect(map.socketMap[ID]).toBeUndefined();
  });

  it('should return an array of all websockets', () => {
    const map = SocketMap.SINGLETON();
    const expectedClients = [{ order: '1' }, { order: '2' }, { order: '3' }, { order: '4' }, { order: '5' }, { order: '6' }, { order: '7' }]
    map.add(expectedClients[0], "OIOS");
    map.add(expectedClients[1], "OSNA");
    map.add(expectedClients[2], "OPWQ");
    map.add(expectedClients[3], "SMIA");
    map.add(expectedClients[4], "POWQ");
    map.add(expectedClients[5], "CNAS");
    map.add(expectedClients[6], "WOPQ");

    const allClients = map.getAllClients();

    allClients.sort((a, b) => {
      if (a.order < b.order) {
        return -1;
      } else {
        return 1;
      }
    });

    expect(allClients).toEqual(expectedClients);
  });

  it('should remove a client from itself wherever it is', () => {
    const map = SocketMap.SINGLETON();
    const expectedClients = [{ order: '1' }, { order: '2' }, { order: '3' }, { order: '4' }, { order: '5' }, { order: '6' }, { order: '7' }]
    map.add(expectedClients[0], "OIOS");
    map.add(expectedClients[1], "OSNA");
    map.add(expectedClients[2], "OPWQ");
    map.add(expectedClients[3], "SMIA");
    map.add(expectedClients[4], "POWQ");
    map.add(expectedClients[5], "CNAS");
    map.add(expectedClients[6], "WOPQ");

    map.removeWebsocket(expectedClients[3]);

    expect(map.getClients('SMIA')).toEqual([]);
  });
});