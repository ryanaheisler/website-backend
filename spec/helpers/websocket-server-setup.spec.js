const td = require('testdouble');
const proxyquire = require('proxyquire');
const SocketMap = require('../../src/helpers/socket-map');

describe('Websocket Server Setup', () => {

    let setup
    let gameID;
    let mockWebsocketServer, mockWebsocket, websocketRequest;
    let mockSetIntervalWrapper;
    let mockPruneMap;
    let originalConsoleLog;

    beforeEach(() => {
        gameID = "ISAS"

        mockWebsocketServer = td.object(['on']);
        mockWebsocket = td.object(['on', 'close']);
        websocketRequest = {
            url: `ws://localhost:8000?gameID=${gameID}`,
            headers: {
                origin: "some origin"
            }
        }

        mockSetIntervalWrapper = td.function('setInterval');

        mockPruneMap = td.function('prune and ping');

        setup = proxyquire('../../src/helpers/websocket-server-setup', {
            './set-interval-wrapper': mockSetIntervalWrapper,
            './prune-websockets': mockPruneMap
        });

        originalConsoleLog = console.log;
        console.log = () => { };
    });

    afterEach(() => {
        console.log = originalConsoleLog;
        SocketMap.instance = null;
    });

    describe('on connection', () => {
        let websocketServerOnCaptor;
        beforeEach(() => {
            setup(mockWebsocketServer);
            websocketServerOnCaptor = td.matchers.captor();
            td.verify(mockWebsocketServer.on('connection', websocketServerOnCaptor.capture()));
        });

        it('should set up the connecting Websocket', () => {
            websocketServerOnCaptor.value(mockWebsocket, websocketRequest);

            expect(mockWebsocket.isAlive).toBeTrue();

            const socketMap = SocketMap.SINGLETON();
            expect(socketMap.getClients(gameID)).toEqual([mockWebsocket]);

            const websocketOnMessageCaptor = td.matchers.captor();
            td.verify(mockWebsocket.on('message', websocketOnMessageCaptor.capture()));

            websocketOnMessageCaptor.value();
            td.verify(mockWebsocket.close(4506, "No messages allowed"));
        });

        it('should renew websocket\'s isAlive on \'pong\'', () => {
            websocketServerOnCaptor.value(mockWebsocket, websocketRequest);

            const websocketOnPongCaptor = td.matchers.captor();
            td.verify(mockWebsocket.on('pong', websocketOnPongCaptor.capture()));

            mockWebsocket.isAlive = false;
            websocketOnPongCaptor.value();

            expect(mockWebsocket.isAlive).toBeTrue();
        });

        it('should prune the websocket map on an interval', () => {
            const intervalCaptor = td.matchers.captor();
            td.verify(mockSetIntervalWrapper(intervalCaptor.capture(), 15000));

            intervalCaptor.value();

            td.verify(mockPruneMap());
        });
    });
});