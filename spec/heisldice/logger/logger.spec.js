const logger = require('../../../src/logger/logger');
let td = require('testdouble');

describe('Logger', () => {

    let originalConsoleError = console.error;

    beforeEach(() => {
        console.error = td.function();
    });

    afterEach(() => {
        console.error = originalConsoleError;
    });

    it('should call console.error with error message', () => {
        const message = "this is the message";

        const error = new Error(message);
        logger.error(error);

        td.verify(console.error(`ERROR: ${error.message}`));
    });
});