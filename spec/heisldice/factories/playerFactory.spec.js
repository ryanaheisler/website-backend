const factory = require('../../../src/heisldice/factories/playerFactory');
const categories = require('../../../src/heisldice/categories');
const scorecardMaker = require('../../../src/heisldice/handlers/blank-scorecard-handler');

describe('PlayerFactory', () => {
  it('should build a new player from frontend name and index', () => {
    const name = "Hankry Jay"
    const index = 0
    const diceColor = "#fe09a1";
    const player = factory.buildNewPlayer(name, index, diceColor);

    expect(player.name).toEqual('Hankry Jay');
    expect(player.turnsRemaining).toEqual(Object.keys(categories).length);
    expect(player.rollsRemaining).toEqual(3);
    expect(player.scorecard).toEqual(scorecardMaker.getScorecard());
    expect(player.previousDice).toEqual([1, 2, 3, 4, 5]);
    expect(player.index).toEqual(0);
    expect(player.myTurn).toBe(true);
    expect(player.diceColor).toBe(diceColor);
  });

  it('should build a new player whose turn it is not', () => {
    const name = "Jankry Hay"
    const index = 4
    const player = factory.buildNewPlayer(name, index);

    expect(player.name).toEqual('Jankry Hay');
    expect(player.turnsRemaining).toEqual(Object.keys(categories).length);
    expect(player.rollsRemaining).toEqual(3);
    expect(player.scorecard).toEqual(scorecardMaker.getScorecard());
    expect(player.previousDice).toEqual([1, 2, 3, 4, 5]);
    expect(player.index).toEqual(index);
    expect(player.myTurn).toBe(false);
    expect(player.diceColor).toBeUndefined();
  });
});