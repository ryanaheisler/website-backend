const td = require('testdouble');
const proxyquire = require('proxyquire');


describe('GameFactory', () => {

  const gameID = "HJWI";

  it('should build a new game', () => {
    const mockGenerator = td.object(['getID']);
    td.when(mockGenerator.getID()).thenReturn(gameID);

    const factory = proxyquire("../../../src/heisldice/factories/gameFactory", {
      '../../helpers/game-id-generator': mockGenerator
    });

    const game = factory.build();
    expect(game.players).toEqual([]);
    expect(game.gameID).toEqual(gameID);
    expect(game.startTime).toBeGreaterThanOrEqual(Date.now() - 10);
    expect(game.startTime).toBeLessThan(Date.now() + 100);
  });
});