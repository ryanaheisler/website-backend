const finder = require('../../../src/heisldice/helpers/next-player-finder');

describe('NextPlayerFinder', () => {
  let player1, player2, player3;
  let game

  beforeEach(() => {
    player1 = {
      name: "One",
      turnsRemaining: 3,
      rollsRemaining: 3,
      scorecard: { "Shmevens": null },
      index: 0,
      myTurn: false
    };
    player2 = { ...player1, name: "Two", index: 1 };
    player3 = { ...player1, name: "Three", index: 2 };

    game = {
      players: [player3, player2, player1]
    }
  });

  describe('When all players have the typical number of turns remaining', () => {
    it('should not change the order of the players in the game', () => {
      player1.myTurn = true;
      finder(game);
      expect(game.players[0]).toBe(player3);
      expect(game.players[1]).toBe(player2);
      expect(game.players[2]).toBe(player1);
    });
    it('should return the next player by index from a game', () => {
      player1.myTurn = true;
      player1.turnsRemaining -= 1;

      expect(finder(game)).toBe(player2);
    });

    it('should return the next player by index from a game - back to index 0', () => {
      player3.myTurn = true;
      player3.turnsRemaining -= 1;

      expect(finder(game)).toBe(player1);
    });
  });
  describe('When at least one player is behind out of order', () => {
    it('should return the player with the most rolls remaining - 1', () => {
      player1.turnsRemaining = 4;
      player1.myTurn = true;
      player2.turnsRemaining = 2;
      player3.turnsRemaining = 2;

      expect(finder(game)).toBe(player1);
    });
    it('should return the player with the most rolls remaining - 2', () => {
      player1.turnsRemaining = 3;
      player1.myTurn = true;
      player2.turnsRemaining = 2;
      player3.turnsRemaining = 2;

      expect(finder(game)).toBe(player1);
    });
    it('should return the player with the most rolls remaining - 3', () => {
      player1.turnsRemaining = 2;
      player1.myTurn = true;
      player2.turnsRemaining = 2;
      player3.turnsRemaining = 2;

      expect(finder(game)).toBe(player2);
    });
    it('should return the player with the most rolls remaining - 4', () => {
      player1.turnsRemaining = 1;
      player1.myTurn = true;
      player2.turnsRemaining = 2;
      player3.turnsRemaining = 2;

      expect(finder(game)).toBe(player2);
    });
    it('should return the player with the most rolls remaining - 5', () => {
      player1.turnsRemaining = 1;
      player2.turnsRemaining = 2;
      player2.myTurn = true;
      player3.turnsRemaining = 2;

      expect(finder(game)).toBe(player3);
    });
    it('should return the player with the most rolls remaining - 6', () => {
      player1.turnsRemaining = 1;
      player1.myTurn = true;
      player2.turnsRemaining = 2;
      player3.turnsRemaining = 3;

      expect(finder(game)).toBe(player3);
    });
    it('should return the player with the most rolls remaining - 7', () => {
      player1.turnsRemaining = 1;
      player2.turnsRemaining = 2;
      player3.turnsRemaining = 2;
      player3.myTurn = true;

      expect(finder(game)).toBe(player2);
    });
    it('should return the player with the most rolls remaining - 8', () => {
      player1.turnsRemaining = 2;
      player2.turnsRemaining = 3;
      player3.turnsRemaining = 2;
      player3.myTurn = true;

      expect(finder(game)).toBe(player2);
    });
  });

});
