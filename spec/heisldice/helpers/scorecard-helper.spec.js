const helper = require('../../../src/heisldice/helpers/scorecard-helper');
const ScorecardMaker = require('../../../src/heisldice/handlers/blank-scorecard-handler');
const cats = require('../../../src/heisldice/categories');

describe('ScorecardHelper', () => {

  let scorecard;

  beforeEach(() => {
    scorecard = ScorecardMaker.getScorecard();
  });

  describe('Section totals', () => {
    const topTestCases = [
      { category: cats.ACES, dice: [1, 1, 3, 4, 1], score: 3 },
      { category: cats.TWOS, dice: [2, 2, 3, 4, 2], score: 6 },
      { category: cats.THREES, dice: [1, 3, 3, 3, 3], score: 12 },
      { category: cats.FOURS, dice: [4, 1, 3, 4, 1], score: 8 },
      { category: cats.FIVES, dice: [5, 5, 5, 4, 1], score: 15 },
      { category: cats.SIXES, dice: [6, 1, 3, 4, 1], score: 6 },
    ]
    topTestCases.forEach(test => {
      it(`${test.category} should go toward the total of the top section`, () => {
        const newScorecard = helper.getUpdatedScorecard(scorecard, test.category, test.dice);

        expect(newScorecard.topSectionTotal).toEqual(test.score);
        expect(newScorecard.categories[test.category].score).toEqual(test.score);
      });
    });

    const bottomTestCases = [
      { category: cats.THREE_OF_A_KIND, dice: [1, 1, 3, 4, 1], score: 10 },
      { category: cats.FOUR_OF_A_KIND, dice: [3, 3, 3, 4, 3], score: 16 },
      { category: cats.FULL_HOUSE, dice: [1, 1, 4, 4, 1], score: 25 },
      { category: cats.SMALL_STRAIGHT, dice: [1, 6, 3, 4, 2], score: 30 },
      { category: cats.LARGE_STRAIGHT, dice: [1, 5, 3, 4, 2], score: 40 },
      { category: cats.YAHTZEE, dice: [1, 1, 1, 1, 1], score: 50 },
      { category: cats.CHANCE, dice: [6, 1, 3, 4, 1], score: 15 },
    ]
    bottomTestCases.forEach(test => {
      it(`${test.category} should go toward the total of the bottom section`, () => {
        const newScorecard = helper.getUpdatedScorecard(scorecard, test.category, test.dice);

        expect(newScorecard.bottomSectionTotal).toEqual(test.score);
        expect(newScorecard.categories[test.category].score).toEqual(test.score);
      });
    });
  });

  describe('Top Section bonuses', () => {
    it('should be 35 if the top section total is 63 or greater', () => {
      scorecard.categories[cats.ACES].score = 3;
      scorecard.categories[cats.TWOS].score = 6;
      scorecard.categories[cats.THREES].score = 9;
      scorecard.categories[cats.FOURS].score = 12;
      scorecard.categories[cats.FIVES].score = 15;
      scorecard.categories[cats.SIXES].score = 18;

      const newScorecard = helper.getUpdatedScorecard(scorecard, cats.THREE_OF_A_KIND, [1, 2, 1, 3, 1]);

      expect(newScorecard.topSectionBonus).toEqual(35);
      expect(newScorecard.topSectionTotal).toEqual(98);
    });

    it('should be 0 if the top section total is 62 or less', () => {
      scorecard.categories[cats.ACES].score = 2;
      scorecard.categories[cats.TWOS].score = 6;
      scorecard.categories[cats.THREES].score = 9;
      scorecard.categories[cats.FOURS].score = 12;
      scorecard.categories[cats.FIVES].score = 15;
      scorecard.categories[cats.SIXES].score = 18;

      const newScorecard = helper.getUpdatedScorecard(scorecard, cats.THREE_OF_A_KIND, [1, 2, 1, 3, 1]);

      expect(newScorecard.topSectionBonus).toEqual(0);
      expect(newScorecard.topSectionTotal).toEqual(62);
    });
  });

  describe('Yahtzee Bonuses', () => {
    it('should add 100 for each yahtzee when yahtzee score is 50', () => {
      let newScorecard;

      newScorecard = helper.getUpdatedScorecard(scorecard, cats.YAHTZEE, [3, 3, 3, 3, 3]);
      expect(newScorecard.categories[cats.YAHTZEE].score).toEqual(50);
      expect(newScorecard.extraYahtzeeBonusTotal).toEqual(0);
      expect(newScorecard.bottomSectionTotal).toEqual(50);

      newScorecard = helper.getUpdatedScorecard(newScorecard, cats.THREE_OF_A_KIND, [3, 3, 3, 3, 3]);
      expect(newScorecard.categories[cats.THREE_OF_A_KIND].score).toEqual(15);
      expect(newScorecard.extraYahtzeeBonusTotal).toEqual(100);
      expect(newScorecard.bottomSectionTotal).toEqual(165);

      newScorecard = helper.getUpdatedScorecard(newScorecard, cats.TWOS, [2, 2, 2, 2, 2]);
      expect(newScorecard.categories[cats.TWOS].score).toEqual(10);
      expect(newScorecard.extraYahtzeeBonusTotal).toEqual(200);
      expect(newScorecard.bottomSectionTotal).toEqual(265);
    });

    it('should not give 100 point bonus if 5 of a kind is taken in another category and yahtzee is blank', () => {
      let newScorecard;

      newScorecard = helper.getUpdatedScorecard(scorecard, cats.THREES, [3, 3, 3, 3, 3]);
      expect(newScorecard.categories[cats.THREES].score).toEqual(15);
      expect(newScorecard.categories[cats.YAHTZEE].score).toEqual(null);
      expect(newScorecard.extraYahtzeeBonusTotal).toEqual(0);
      expect(newScorecard.topSectionTotal).toEqual(15);
      expect(newScorecard.bottomSectionTotal).toEqual(0);
    });
  });

  describe('Complete Scorecards', () => {
    it('good score', () => {
      scorecard.categories[cats.ACES].score = 3;
      scorecard.categories[cats.TWOS].score = 6;
      scorecard.categories[cats.THREES].score = 9;
      scorecard.categories[cats.FOURS].score = 12;
      scorecard.categories[cats.FIVES].score = 15;
      scorecard.categories[cats.SIXES].score = 24;

      scorecard.categories[cats.THREE_OF_A_KIND].score = 24;
      scorecard.categories[cats.FOUR_OF_A_KIND].score = 29;
      scorecard.categories[cats.FULL_HOUSE].score = 25;
      scorecard.categories[cats.SMALL_STRAIGHT].score = 30;
      scorecard.categories[cats.LARGE_STRAIGHT].score = 40;
      scorecard.categories[cats.YAHTZEE].score = 50;
      scorecard.categories[cats.CHANCE].score = null;

      const newScorecard = helper.getUpdatedScorecard(scorecard, cats.CHANCE, [3, 3, 3, 3, 3]);

      expect(newScorecard.topSectionBonus).toEqual(35);
      expect(newScorecard.topSectionTotal).toEqual(104);
      expect(newScorecard.extraYahtzeeBonusTotal).toEqual(100);
      expect(newScorecard.bottomSectionTotal).toEqual(313);
      expect(newScorecard.total).toEqual(417);
    });

    it('bad score', () => {
      scorecard.categories[cats.ACES].score = 2;
      scorecard.categories[cats.TWOS].score = 6;
      scorecard.categories[cats.THREES].score = 3;
      scorecard.categories[cats.FOURS].score = 12;
      scorecard.categories[cats.FIVES].score = 10;
      scorecard.categories[cats.SIXES].score = 24;

      scorecard.categories[cats.THREE_OF_A_KIND].score = 15;
      scorecard.categories[cats.FOUR_OF_A_KIND].score = 0;
      scorecard.categories[cats.FULL_HOUSE].score = 25;
      scorecard.categories[cats.SMALL_STRAIGHT].score = 30;
      scorecard.categories[cats.LARGE_STRAIGHT].score = 0;
      scorecard.categories[cats.YAHTZEE].score = 0;
      scorecard.categories[cats.CHANCE].score = null;

      const newScorecard = helper.getUpdatedScorecard(scorecard, cats.CHANCE, [3, 3, 3, 3, 3]);

      expect(newScorecard.topSectionBonus).toEqual(0);
      expect(newScorecard.topSectionTotal).toEqual(57);
      expect(newScorecard.extraYahtzeeBonusTotal).toEqual(0);
      expect(newScorecard.bottomSectionTotal).toEqual(85);
      expect(newScorecard.total).toEqual(142);
    });
  });
});