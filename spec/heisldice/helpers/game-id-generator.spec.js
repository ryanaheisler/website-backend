const generator = require("../../../src/helpers/game-id-generator");

describe('GameIdGenerator', () => {

  let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

  it('should generate 4-digit random strings in all caps', () => {
    const ids = []
    for (let index = 0; index < 10; index++) {
      ids.push(generator.getID());
    }
    expect(ids.length).toEqual(10);

    ids.sort();
    ids.forEach((id, index) => {
      expect(id.length).toEqual(4);
      if (index > 0) {
        expect(id).not.toEqual(ids[index - 1]);
      }
      id.split('').forEach(letter => {
        expect(characters.includes(letter)).toBeTrue();
      });
    });
  });
});