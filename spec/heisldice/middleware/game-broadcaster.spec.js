const td = require('testdouble');
const proxyquire = require('proxyquire');
const SocketMap = require('../../../src/helpers/socket-map');

describe('Game Broadcaster', () => {
    let gameID, otherGameID;
    let Broadcaster;
    let databaseCommunicator, request, response;
    let websocketMap;
    let mockTimeout;

    beforeEach(() => {
        gameID = "IOEW"
        otherGameID = "SOPQ"
        request = {
            params: { gameID: gameID },
            game: { gameID: gameID }
        };
        response = {}

        websocketMap = SocketMap.SINGLETON()
        websocketMap.add(td.object(['send']), gameID);
        websocketMap.add(td.object(['send']), gameID);
        websocketMap.add(td.object(['send']), gameID);

        websocketMap.add(td.object(['send']), otherGameID);

        databaseCommunicator = { 'db': 'comm' };

        mockTimeout = td.function();

        Broadcaster = proxyquire('../../../src/heisldice/middleware/game_broadcaster', {
            '../../helpers/set-timeout-wrapper': mockTimeout
        });
    });
    afterEach(() => {
        SocketMap.instance = null
    });
    it('should get the current game with given ID and broadcast it to all web-socket clients', (done) => {
        const broadcastMiddleware = Broadcaster(databaseCommunicator);
        broadcastMiddleware(request, response);

        const callbackCaptor = td.matchers.captor();
        td.verify(mockTimeout(callbackCaptor.capture(), 100));

        callbackCaptor.value();

        const clients = websocketMap.getClients(gameID);
        td.verify(clients[0].send(JSON.stringify(request.game)));
        td.verify(clients[1].send(JSON.stringify(request.game)));
        td.verify(clients[2].send(JSON.stringify(request.game)));

        td.verify(websocketMap.getClients(otherGameID)[0].send(td.matchers.anything()), { times: 0 });
        done()
    });
});