const PlayerExtractor = require('../../../src/heisldice/middleware/player-body-extractor');
const td = require('testdouble');

describe('PlayerExtractorFromBody', () => {

    let request;
    let response;
    let responseWithStatus;
    let next;
    let middleware;
    let game;

    beforeEach(() => {
        response = td.object(['status']);
        responseWithStatus = td.object(['send']);
        next = td.function('next');

        middleware = PlayerExtractor();
    });

    it('should get game and add it to the request', (done) => {
        game = {
            players: [
                {
                    name: "grumpy jane"
                },
                {
                    name: "jimmy jimmy"
                }
            ]
        }

        request = {
            body: {
                playerName: game.players[1].name
            },
            game: game
        }

        middleware(request, response, next);

        setTimeout(() => {
            td.verify(next());
            expect(request.player).toBe(game.players[1]);
            done();
        }, 100);
    });

    it('should ignore missing player name and call next', (done) => {
        game = {
            players: [{ name: "jimmy" }]
        }

        request = {
            body: {
                playerName: "another name"
            },
            game: game
        }

        middleware(request, response, next);

        setTimeout(() => {
            td.verify(next());
            done();
        }, 100);
    });
});