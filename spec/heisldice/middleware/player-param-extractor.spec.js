const PlayerExtractor = require('../../../src/heisldice/middleware/player-param-extractor');
const td = require('testdouble');
const constants = require('../../../src/heisldice/constants');

describe('PlayerExtractorFromParams', () => {

  let request;
  let response;
  let responseWithStatus;
  let next;
  let middleware;
  let game;

  beforeEach(() => {
    response = td.object(['status']);
    responseWithStatus = td.object(['send']);
    next = td.function('next');

    middleware = PlayerExtractor();
  });

  it('should get game and add it to the request', (done) => {
    game = {
      players: [
        {
          name: "grumpy jane"
        },
        {
          name: "jimmy jimmy"
        }
      ]
    }

    request = {
      params: {
        name: game.players[1].name
      },
      game: game
    }

    middleware(request, response, next);

    setTimeout(() => {
      td.verify(next());
      expect(request.player).toBe(game.players[1]);
      done();
    }, 100);
  });

  it('should respond with error code if there is no current game', (done) => {
    game = {
      players: []
    }

    request = {
      params: {
        name: "looooooooooooke"
      },
      game: game
    }
    const errorMessage = constants.ERROR_MESSAGES.PLAYER_NOT_FOUND;

    td.when(response.status(404)).thenReturn(responseWithStatus);

    middleware(request, response, next);

    setTimeout(() => {
      td.verify(responseWithStatus.send({ error: errorMessage }));
      done();
    }, 100);
  });
});