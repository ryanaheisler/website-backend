const IsItMyTurn = require('../../../src/heisldice/middleware/is-it-my-turn');
const td = require('testdouble');
const constants = require('../../../src/heisldice/constants');

describe('IsItMyTurn', () => {

  let request;
  let response;
  let responseWithStatus;
  let next;
  let databaseCommunicator;
  let middleware;
  let game;

  beforeEach(() => {
    request = {
      params: {
        gameID: "AMZF",
        name: "The Gorephan"
      }
    }
    response = td.object(['status']);
    responseWithStatus = td.object(['send']);
    next = td.function('next');

    databaseCommunicator = td.object(['getCurrentGame']);
    middleware = IsItMyTurn(databaseCommunicator);

  });

  it('should get game and call next if it is the player\'s turn', (done) => {
    game = {
      players: [{
        name: request.params.name,
        myTurn: true
      }],
      gameID: request.params.gameID
    }

    td.when(databaseCommunicator.getCurrentGame(request.params.gameID)).thenResolve(game);

    middleware(request, response, next);

    setTimeout(() => {
      td.verify(next());
      done();
    }, 100);
  });

  it('should respond with error code if it is NOT the player\'s turn', (done) => {
    game = {
      players: [{
        name: request.params.name,
        myTurn: false
      }],
      gameID: request.params.gameID
    }

    td.when(response.status(422)).thenReturn(responseWithStatus);
    td.when(databaseCommunicator.getCurrentGame(request.params.gameID)).thenResolve(game);

    middleware(request, response, next);

    setTimeout(() => {
      const errorMessage = constants.ERROR_MESSAGES.NOT_YOUR_TURN(request.params.name);
      td.verify(responseWithStatus.send({ error: errorMessage }));
      done();
    }, 100);
  });
});