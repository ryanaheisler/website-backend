const GameExtractor = require('../../../src/heisldice/middleware/game-extractor');
const td = require('testdouble');
const constants = require('../../../src/heisldice/constants');

describe('GameExtractor', () => {

  let request;
  let response;
  let responseWithStatus;
  let next;
  let databaseCommunicator;
  let middleware;
  let game;

  beforeEach(() => {
    request = {
      params: {
        gameID: "WERT"
      }
    }
    response = td.object(['status']);
    responseWithStatus = td.object(['send']);
    next = td.function('next');

    databaseCommunicator = td.object(['getCurrentGame']);
    middleware = GameExtractor(databaseCommunicator);
  });

  it('should get game and add it to the request', (done) => {
    game = {
      gameID: request.params.gameID
    }

    td.when(databaseCommunicator.getCurrentGame(request.params.gameID)).thenResolve(game);

    middleware(request, response, next);

    setTimeout(() => {
      td.verify(next());
      expect(request.game).toBe(game);
      done();
    }, 100);
  });

  it('should respond with error code if there is no current game', (done) => {
    const errorMessage = constants.ERROR_MESSAGES.GAME_NOT_FOUND(request.params.gameID);

    td.when(databaseCommunicator.getCurrentGame(request.params.gameID)).thenResolve(undefined);
    td.when(response.status(404)).thenReturn(responseWithStatus);

    middleware(request, response, next);

    setTimeout(() => {
      td.verify(responseWithStatus.send({ error: errorMessage }));
      done();
    }, 100);
  });
});