const proxyquire = require('proxyquire');
const td = require('testdouble');
const constants = require('../../../src/heisldice/constants');

describe('AddPlayerHandler', () => {
  let handler;
  let gameID;
  let currentGame;
  let mockDBCommunicator;
  let mockPlayerFactory;
  let builtPlayer;
  let diceColor;

  beforeEach(() => {
    mockDBCommunicator = td.object(['addPlayer', 'getCurrentGame', 'insertGame', 'updateGame', 'close']);

    gameID = "GHHF"
    diceColor = "#34ae00";
    currentGame = {
      gameID: gameID,
      players: [
        { name: "Ryan" }
      ]
    }
    builtPlayer = { name: 'jimbo', diceColor: diceColor };
    mockPlayerFactory = td.object(['buildNewPlayer']);

    handler = proxyquire("../../../src/heisldice/handlers/add-player-handler", {
      '../factories/playerFactory': mockPlayerFactory,
    });
  });

  it('should add player to current game', () => {
    td.when(mockPlayerFactory.buildNewPlayer(builtPlayer.name, currentGame.players.length, builtPlayer.diceColor))
      .thenReturn(builtPlayer);

    const updatedGame = {
      players: [{ name: "Ryan" }, builtPlayer],
      gameID: gameID
    };
    const gameCaptor = td.matchers.captor();
    td.when(mockDBCommunicator.updateGame(gameCaptor.capture())).thenResolve();

    return handler.execute(currentGame, builtPlayer.name, mockDBCommunicator, builtPlayer.diceColor)
      .then((player) => {
        expect(gameCaptor.value).toEqual(updatedGame);
        expect(player).toEqual(builtPlayer);
      });
  });

  it('should reject if there is already a player with the specified name', () => {
    return handler.execute(currentGame, "Ryan", mockDBCommunicator).then(() => {
      fail('This shouldn\'t have resolved');
    }).catch((error) => {
      expect(error.message).toEqual(constants.ERROR_MESSAGES.PLAYER_NAME_ALREADY_USED("Ryan"));
    });
  });
});