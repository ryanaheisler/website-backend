const td = require('testdouble');
const proxyquire = require('proxyquire');

describe('SetScoreHandler', () => {

  let databaseCommunicator;
  let game;
  let currentPlayer, nextPlayer;
  let dice;
  let score;
  let category;
  let expectedUpatedCurrentPlayer;
  let expectedUpatedNextPlayer;
  let mockScorecardHelper
  let handler;

  beforeEach(() => {
    currentPlayer = {
      name: "Ryan",
      turnsRemaining: 3,
      rollsRemaining: 2,
      scorecard: { "Shmevens": null },
      index: 0,
      myTurn: true
    };
    nextPlayer = {
      name: "nyaR",
      turnsRemaining: 3,
      rollsRemaining: 3,
      scorecard: { "Shmevens": null },
      index: 1,
      myTurn: false
    };

    game = {
      gameID: "GAME",
      players: [
        currentPlayer,
        nextPlayer
      ]
    }
    category = {
      score: 113,
      selected: false,
      fitsCurrentDice: false,
      description: "Shmevens"
    }
    dice = [4, 2, 3, 3, 1];
    score = 14;

    databaseCommunicator = td.object(['getCurrentGame', 'updateGame']);

    const updatedScorecard = { "Shmevens": { score: score } };
    expectedUpatedCurrentPlayer = {
      name: currentPlayer.name,
      turnsRemaining: 2,
      rollsRemaining: 3,
      scorecard: updatedScorecard,
      index: 0,
      myTurn: false
    };
    expectedUpatedNextPlayer = {
      name: currentPlayer.name,
      turnsRemaining: 3,
      rollsRemaining: 3,
      scorecard: updatedScorecard,
      index: 1,
      myTurn: true
    };

    const expectedUpdatedGame = {
      players: [expectedUpatedCurrentPlayer, expectedUpatedNextPlayer],
    };
    td.when(databaseCommunicator.updateGame(expectedUpdatedGame)).thenResolve();

    mockScorecardHelper = td.object(['getUpdatedScorecard']);
    td.when(mockScorecardHelper.getUpdatedScorecard(currentPlayer.scorecard, category.description, dice)).thenReturn(updatedScorecard);

    handler = proxyquire('../../../src/heisldice/handlers/set-score-handler', {
      '../helpers/scorecard-helper': mockScorecardHelper
    });
  });

  it('should update score for category with given dice', async () => {
    const results = await handler.execute(game, currentPlayer, category.description, dice, databaseCommunicator);

    expect(results).toEqual(expectedUpatedCurrentPlayer);
    expect(currentPlayer.myTurn).toBe(false);
    expect(nextPlayer.myTurn).toBe(true)
  });

  it('should reject if update game fails', async () => {
    td.when(databaseCommunicator.updateGame(td.matchers.anything())).thenReject(new Error("NOPE"));
    try {
      await handler.execute(game, currentPlayer, category.description, dice, databaseCommunicator);
      fail('this should have rejected');
    } catch (error) {
      expect(error.message).toEqual("NOPE");
    }
  });
});