const rules = require('../../../src/heisldice/handlers/rules');

describe('Rules', () => {

  describe('(numeric)', () => {

    const calculateScore = (numeric, rollValues) => {
      let score = 0;
      rollValues.forEach(value => {
        if (value === numeric) {
          score += value;
        }
      });
      return score;
    }

    const checkRule = (numeric, rule) => {
      const roll1 = [1, 3, 1, 1, 4];
      const score1 = calculateScore(numeric, roll1);
      expect(rule(roll1)).toEqual(score1);

      const roll2 = [4, 2, 1, 2, 6];
      const score2 = calculateScore(numeric, roll2);
      expect(rule(roll2)).toEqual(score2);

      const roll3 = [6, 6, 5, 2, 5];
      const score3 = calculateScore(numeric, roll3);
      expect(rule(roll3)).toEqual(score3);

      const roll4 = [1, 2, 3, 4, 1];
      const score4 = calculateScore(numeric, roll4);
      expect(rule(roll4)).toEqual(score4);
    }

    describe('Aces', () => {
      it('should return correct score', () => {
        checkRule(1, rules.aces)
      });
    });
    describe('Twos', () => {
      it('should return correct score', () => {
        checkRule(2, rules.twos)
      });
    });
    describe('Threes', () => {
      it('should return correct score', () => {
        checkRule(3, rules.threes)
      });
    });
    describe('Fours', () => {
      it('should return correct score', () => {
        checkRule(4, rules.fours)
      });
    });
    describe('Fives', () => {
      it('should return correct score', () => {
        checkRule(5, rules.fives)
      });
    });
    describe('Sixes', () => {
      it('should return correct score', () => {
        checkRule(6, rules.sixes)
      });
    });
  });

  describe('(poker)', () => {
    describe('3-of-a-kind', () => {
      it('should return correct score', () => {
        const score1 = rules.threeOfAKind([4, 3, 2, 4, 4]);
        const score2 = rules.threeOfAKind([1, 3, 2, 3, 3]);
        const score3 = rules.threeOfAKind([6, 2, 2, 4, 4]);
        const score4 = rules.threeOfAKind([6, 6, 5, 6, 6]);

        expect(score1).toEqual(17);
        expect(score2).toEqual(12);
        expect(score3).toEqual(0);
        expect(score4).toEqual(29);
      });
    });
    describe('4-of-a-kind', () => {
      it('should return correct score', () => {
        const score1 = rules.fourOfAKind([4, 3, 2, 4, 4]);
        const score2 = rules.fourOfAKind([1, 3, 3, 3, 3]);
        const score3 = rules.fourOfAKind([2, 2, 2, 2, 2]);
        const score4 = rules.fourOfAKind([6, 6, 5, 1, 6]);

        expect(score1).toEqual(0);
        expect(score2).toEqual(13);
        expect(score3).toEqual(10);
        expect(score4).toEqual(0);
      });
    });
    describe('Full House', () => {
      it('should return correct score', () => {
        const score1 = rules.fullHouse([4, 3, 3, 4, 4]);
        const score2 = rules.fullHouse([1, 3, 3, 3, 3]);
        const score3 = rules.fullHouse([2, 2, 2, 2, 2]);
        const score4 = rules.fullHouse([6, 6, 5, 1, 6]);

        expect(score1).toEqual(25);
        expect(score2).toEqual(0);
        expect(score3).toEqual(25);
        expect(score4).toEqual(0);
      });
    });
    describe('Small Straight', () => {
      it('should return correct score', () => {
        const dice1 = [5, 1, 4, 2, 3];
        const dice2 = [4, 2, 5, 1, 6];
        const dice3 = [1, 2, 5, 2, 2];
        const dice4 = [3, 4, 5, 6, 1];
        const dice5 = [3, 4, 5, 4, 2];
        const dice6 = [1, 6, 3, 4, 2];

        const score1 = rules.smallStraight(dice1);
        const score2 = rules.smallStraight(dice2);
        const score3 = rules.smallStraight(dice3);
        const score4 = rules.smallStraight(dice4);
        const score5 = rules.smallStraight(dice5);
        const score6 = rules.smallStraight(dice6);

        expect(score1).toEqual(30);
        expect(score2).toEqual(0);
        expect(score3).toEqual(0);
        expect(score4).toEqual(30);
        expect(score5).toEqual(30);
        expect(score6).toEqual(30);

        expect(dice1).toEqual([5, 1, 4, 2, 3]);
        expect(dice2).toEqual([4, 2, 5, 1, 6]);
        expect(dice3).toEqual([1, 2, 5, 2, 2]);
        expect(dice4).toEqual([3, 4, 5, 6, 1]);
        expect(dice5).toEqual([3, 4, 5, 4, 2]);
        expect(dice6).toEqual([1, 6, 3, 4, 2]);
      });
    });
    describe('Large Straight', () => {
      it('should return correct score', () => {
        const dice1 = [5, 1, 4, 2, 3];
        const dice2 = [4, 2, 3, 1, 6];
        const dice3 = [1, 2, 5, 2, 2];
        const dice4 = [3, 4, 5, 6, 2];

        const score1 = rules.largeStraight(dice1);
        const score2 = rules.largeStraight(dice2);
        const score3 = rules.largeStraight(dice3);
        const score4 = rules.largeStraight(dice4);

        expect(score1).toEqual(40);
        expect(score2).toEqual(0);
        expect(score3).toEqual(0);
        expect(score4).toEqual(40);

        expect(dice1).toEqual([5, 1, 4, 2, 3]);
        expect(dice2).toEqual([4, 2, 3, 1, 6]);
        expect(dice3).toEqual([1, 2, 5, 2, 2]);
        expect(dice4).toEqual([3, 4, 5, 6, 2]);
      });
    });
    describe('Yahtzee!', () => {
      it('should return correct score', () => {
        const score1 = rules.yahtzee([1, 2, 1, 1, 1]);
        const score2 = rules.yahtzee([1, 1, 1, 1, 1]);
        const score3 = rules.yahtzee([1, 2, 5, 2, 2]);
        const score4 = rules.yahtzee([5, 5, 5, 5, 5]);

        expect(score1).toEqual(0);
        expect(score2).toEqual(50);
        expect(score3).toEqual(0);
        expect(score4).toEqual(50);
      });
    });
    describe('Chance', () => {
      it('should return correct score', () => {
        const score1 = rules.chance([5, 1, 4, 2, 3]);
        const score2 = rules.chance([4, 2, 3, 1, 6]);
        const score3 = rules.chance([1, 2, 5, 2, 2]);
        const score4 = rules.chance([3, 4, 5, 6, 2]);

        expect(score1).toEqual(15);
        expect(score2).toEqual(16);
        expect(score3).toEqual(12);
        expect(score4).toEqual(20);
      });
    });
  });
});