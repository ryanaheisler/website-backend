const checker = require('../../../src/heisldice/handlers/yahtzee-exception-checker');
const scorecardMaker = require('../../../src/heisldice/handlers/blank-scorecard-handler');
const cats = require('../../../src/heisldice/categories');

describe('YahtzeeExceptionChecker', () => {

  let potentialScorecard, actualScorecard;
  let dice;

  beforeEach(() => {
    actualScorecard = scorecardMaker.getScorecard();
    potentialScorecard = scorecardMaker.getScorecard();

    actualScorecard.categories[cats.ACES].score = 1;
    actualScorecard.categories[cats.TWOS].score = 2;
    actualScorecard.categories[cats.THREES].score = 3;
    actualScorecard.categories[cats.FOURS].score = 4;
    actualScorecard.categories[cats.FIVES].score = 5;
    actualScorecard.categories[cats.SIXES].score = 6;
    actualScorecard.categories[cats.THREE_OF_A_KIND].score = 15;
    actualScorecard.categories[cats.FOUR_OF_A_KIND].score = 20;
    actualScorecard.categories[cats.FULL_HOUSE].score = 25;
    actualScorecard.categories[cats.SMALL_STRAIGHT].score = 30;
    actualScorecard.categories[cats.LARGE_STRAIGHT].score = 40;
    actualScorecard.categories[cats.YAHTZEE].score = 50;
    actualScorecard.categories[cats.CHANCE].score = 18;
  });

  it('should do nothing if yahtzee score is not 50', () => {
    actualScorecard.categories[cats.YAHTZEE].score = 0;

    const newPotentialScorecard = checker.check(potentialScorecard, actualScorecard, undefined);
    expect(newPotentialScorecard).toEqual(potentialScorecard);
    expect(newPotentialScorecard).not.toBe(potentialScorecard);
  });

  it('should do nothing if dice are not yahtzee', () => {

    const newPotentialScorecard = checker.check(potentialScorecard, actualScorecard, [1, 2, 2, 2, 2]);
    expect(newPotentialScorecard).toEqual(potentialScorecard);
    expect(newPotentialScorecard).not.toBe(potentialScorecard);
  });

  describe('!First Yahtzee', () => {

    beforeEach(() => {
      actualScorecard.categories[cats.YAHTZEE].score = 50;
      dice = [3, 3, 3, 3, 3];
    });

    it('should set all other scores to 0 if the corresponding top-section score is unfilled', () => {
      actualScorecard.categories[cats.THREES].score = null;
      potentialScorecard.categories[cats.THREES].score = 15;
      potentialScorecard.categories[cats.ACES].score = 8

      const expectedPotentialScorecard = scorecardMaker.getScorecard();
      expectedPotentialScorecard.categories[cats.ACES].score = 0;
      expectedPotentialScorecard.categories[cats.TWOS].score = 0;
      expectedPotentialScorecard.categories[cats.THREES].score = 15;
      expectedPotentialScorecard.categories[cats.FOURS].score = 0;
      expectedPotentialScorecard.categories[cats.FIVES].score = 0;
      expectedPotentialScorecard.categories[cats.SIXES].score = 0;
      expectedPotentialScorecard.categories[cats.THREE_OF_A_KIND].score = 0;
      expectedPotentialScorecard.categories[cats.FOUR_OF_A_KIND].score = 0;
      expectedPotentialScorecard.categories[cats.FULL_HOUSE].score = 0;
      expectedPotentialScorecard.categories[cats.SMALL_STRAIGHT].score = 0;
      expectedPotentialScorecard.categories[cats.LARGE_STRAIGHT].score = 0;
      expectedPotentialScorecard.categories[cats.YAHTZEE].score = 0;
      expectedPotentialScorecard.categories[cats.CHANCE].score = 0;

      const newPotentialScorecard = checker.check(potentialScorecard, actualScorecard, dice);

      expect(newPotentialScorecard).toEqual(expectedPotentialScorecard);
      expect(newPotentialScorecard).not.toBe(potentialScorecard);
    });

    it('should set small and large straights to maximum score if corresponding top section score is filled', () => {
      actualScorecard.categories[cats.THREES].score = 12;

      const expectedPotentialScorecard = JSON.parse(JSON.stringify(potentialScorecard));
      expectedPotentialScorecard.categories[cats.SMALL_STRAIGHT].score = 30;
      expectedPotentialScorecard.categories[cats.LARGE_STRAIGHT].score = 40;

      const newPotentialScorecard = checker.check(potentialScorecard, actualScorecard, dice);

      expect(newPotentialScorecard).toEqual(expectedPotentialScorecard);
      expect(newPotentialScorecard).not.toBe(potentialScorecard);
    });
  });
});