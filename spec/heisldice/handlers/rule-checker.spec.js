let proxyquire = require('proxyquire');
const cats = require('../../../src/heisldice/categories');

describe('RuleChecker', () => {

  let mockRules;
  let expectedDice;
  let ruleChecker;
  beforeEach(() => {
    mockRules = {
      aces: (dice) => {
        if (dice === expectedDice) {
          return 1;
        }
      },
      twos: (dice) => {
        if (dice === expectedDice) {
          return 2;
        }
      },
      threes: (dice) => {
        if (dice === expectedDice) {
          return 3;
        }
      },
      fours: (dice) => {
        if (dice === expectedDice) {
          return 4;
        }
      },
      fives: (dice) => {
        if (dice === expectedDice) {
          return 5;
        }
      },
      sixes: (dice) => {
        if (dice === expectedDice) {
          return 6;
        }
      },
      threeOfAKind: (dice) => {
        if (dice === expectedDice) {
          return 7;
        }
      },
      fourOfAKind: (dice) => {
        if (dice === expectedDice) {
          return 8;
        }
      },
      fullHouse: (dice) => {
        if (dice === expectedDice) {
          return 9;
        }
      },
      smallStraight: (dice) => {
        if (dice === expectedDice) {
          return 10;
        }
      },
      largeStraight: (dice) => {
        if (dice === expectedDice) {
          return 11;
        }
      },
      yahtzee: (dice) => {
        if (dice === expectedDice) {
          return 12;
        }
      },
      chance: (dice) => {
        if (dice === expectedDice) {
          return 13;
        }
      }
    };
    expectedDice = [1, 1, 3, 5, 6];
    ruleChecker = proxyquire('../../../src/heisldice/handlers/rule-checker', {
      './rules': mockRules
    });
  });

  it('should return matching rules for dice values', () => {
    const results = ruleChecker.check(expectedDice);

    expect(results).toEqual(
      {
        topSectionBonus: 0,
        topSectionTotal: 0,
        extraYahtzeeBonusTotal: 0,
        bottomSectionTotal: 0,
        total: 0,
        categories: {
          "Aces": {
            score: 1,
            section: 0
          },
          "Twos": {
            score: 2,
            section: 0
          },
          "Threes": {
            score: 3,
            section: 0
          },
          "Fours": {
            score: 4,
            section: 0
          },
          "Fives": {
            score: 5,
            section: 0
          },
          "Sixes": {
            score: 6,
            section: 0
          },
          "3 of a Kind": {
            score: 7,
            section: 1
          },
          "4 of a Kind": {
            score: 8,
            section: 1
          },
          "Full House": {
            score: 9,
            section: 1
          },
          "Small Straight": {
            score: 10,
            section: 1
          },
          "Large Straight": {
            score: 11,
            section: 1
          },
          "Yahtzee": {
            score: 12,
            section: 1
          },
          "Chance": {
            score: 13,
            section: 1
          }
        }
      }
    );
  });

  describe('check single rule', () => {
    it('should check given rule by description', () => {
      expect(ruleChecker.checkSingleRule(cats.ACES, expectedDice)).toEqual(1);
      expect(ruleChecker.checkSingleRule(cats.TWOS, expectedDice)).toEqual(2);
      expect(ruleChecker.checkSingleRule(cats.THREES, expectedDice)).toEqual(3);
      expect(ruleChecker.checkSingleRule(cats.FOURS, expectedDice)).toEqual(4);
      expect(ruleChecker.checkSingleRule(cats.FIVES, expectedDice)).toEqual(5);
      expect(ruleChecker.checkSingleRule(cats.SIXES, expectedDice)).toEqual(6);
      expect(ruleChecker.checkSingleRule(cats.THREE_OF_A_KIND, expectedDice)).toEqual(7);
      expect(ruleChecker.checkSingleRule(cats.FOUR_OF_A_KIND, expectedDice)).toEqual(8);
      expect(ruleChecker.checkSingleRule(cats.FULL_HOUSE, expectedDice)).toEqual(9);
      expect(ruleChecker.checkSingleRule(cats.SMALL_STRAIGHT, expectedDice)).toEqual(10);
      expect(ruleChecker.checkSingleRule(cats.LARGE_STRAIGHT, expectedDice)).toEqual(11);
      expect(ruleChecker.checkSingleRule(cats.YAHTZEE, expectedDice)).toEqual(12);
      expect(ruleChecker.checkSingleRule(cats.CHANCE, expectedDice)).toEqual(13);
    });
  });
});