const td = require('testdouble');
const handler = require('../../../src/heisldice/handlers/change-dice-color-handler');

describe('ChangeDiceColorHandler', () => {
    let databaseCommunicator;
    let game;
    let player;
    let color;

    beforeEach(() => {
        databaseCommunicator = td.object(['updateGame']);
        gameID = "GHHF"
        player = { name: "Ryan" };
        game = {
            gameID: gameID,
            players: [
                player
            ]
        }
        color = "#ff0034";
    });

    it('should set the provided color on the player and update the game in the database', async () => {
        const gameCaptor = td.matchers.captor();
        td.when(databaseCommunicator.updateGame(gameCaptor.capture())).thenResolve();

        const updatedGame = {
            gameID: gameID,
            players: [
                { name: "Ryan", diceColor: color }
            ]
        }

        const results = await handler.setColor(game, player, color, databaseCommunicator);

        expect(results).toEqual(updatedGame.players[0]);
        expect(gameCaptor.value).toEqual(updatedGame);
    });
});