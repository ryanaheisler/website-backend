const handler = require('../../../src/heisldice/handlers/blank-scorecard-handler')

describe('BlankScorecardHandler', () => {
  it('should return the blank scorecard', () => {
    const scorecard = handler.getScorecard();

    expect(scorecard).toEqual(
      {
        topSectionBonus: 0,
        topSectionTotal: 0,
        extraYahtzeeBonusTotal: 0,
        bottomSectionTotal: 0,
        total: 0,
        categories: {
          "Aces": {
            score: null,
            section: 0
          },
          "Twos": {
            score: null,
            section: 0
          },
          "Threes": {
            score: null,
            section: 0
          },
          "Fours": {
            score: null,
            section: 0
          },
          "Fives": {
            score: null,
            section: 0
          },
          "Sixes": {
            score: null,
            section: 0
          },
          "3 of a Kind": {
            score: null,
            section: 1
          },
          "4 of a Kind": {
            score: null,
            section: 1
          },
          "Full House": {
            score: null,
            section: 1
          },
          "Small Straight": {
            score: null,
            section: 1
          },
          "Large Straight": {
            score: null,
            section: 1
          },
          "Yahtzee": {
            score: null,
            section: 1
          },
          "Chance": {
            score: null,
            section: 1
          }
        }
      }
    )
  });
});