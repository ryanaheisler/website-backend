const handler = require('../../../src/heisldice/handlers/player-roll-handler');
const td = require('testdouble');

describe('PlayerRollHandler', () => {

  let databaseCommunicator;
  let game;
  let gameID;
  beforeEach(() => {
    databaseCommunicator = td.object(['getCurrentGame', 'updateGame']);
    game = {
      players: [
        { name: "Johnny", rollsRemaining: 0, previousDice: [] },
        { name: "Ryan", rollsRemaining: 2, previousDice: [] },
        { name: "Karen", rollsRemaining: 0, previousDice: [] },
      ],
      gameID: gameID
    };
  });

  it('should update player roll count', async () => {
    const rolledDice = [5, 4, 6, 2, 1];
    const updatedGame = {
      players: [
        { name: "Johnny", rollsRemaining: 0, previousDice: [] },
        { name: "Ryan", rollsRemaining: 1, previousDice: rolledDice },
        { name: "Karen", rollsRemaining: 0, previousDice: [] },
      ],
      gameID: gameID
    };

    const gameCaptor = td.matchers.captor();
    td.when(databaseCommunicator.updateGame(gameCaptor.capture())).thenResolve();

    const results = await handler.roll(game, game.players[1], rolledDice, databaseCommunicator);
    expect(results).toEqual(updatedGame.players[1]);
    expect(gameCaptor.value).toEqual(updatedGame);
  });

  describe('errors', () => {
    it('should reject if update game fails', async () => {
      const error = new Error('Nope!');
      td.when(databaseCommunicator.updateGame(td.matchers.anything())).thenReject(error);

      try {
        await handler.roll(game, game.players[1], [], databaseCommunicator);
        fail('The promise did not reject');
      } catch (resultError) {
        expect(resultError).toBe(error);
      }
    });
  });
});