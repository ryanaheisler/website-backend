const RollHandler = require('../../../src/heisldice/handlers/dice-roller')
let td = require('testdouble');

describe('RollHandler', () => {

  let originalMath;
  let mockRandom;

  beforeAll(() => {
    originalMath = Math;
    mockRandom = td.function();
    Math = {
      random: mockRandom,
      ceil: Math.ceil,
      max: Math.max
    }
  })

  afterAll(() => {
    Math = originalMath;
  })

  it('should return a random set of 5 values', () => {
    const roll1 = [0.16666, 0.643, 0.7622, 0, 0.99999];
    const roll2 = [0.333334, 0.333333, 0.57, 0.325, 0.8621];
    const randomValues = [...roll1, ...roll2];
    td.when(mockRandom()).thenReturn(...randomValues);

    const expectedRoll1 = [1, 4, 5, 1, 6];
    const expectedRoll2 = [3, 2, 4, 2, 6];

    expect(RollHandler.roll([0, 0, 0, 0, 0])).toEqual(expectedRoll1);
    expect(RollHandler.roll([0, 0, 0, 0, 0])).toEqual(expectedRoll2);
  });

  it('should not overwrite reserved dice', () => {
    const roll1 = [0.16666, 0.643, 0.7622, 0, 0.99999];
    const roll2 = [0.333334, 0.333333, 0.57, 0.325, 0.8621];
    const randomValues = [...roll1, ...roll2];
    td.when(mockRandom()).thenReturn(...randomValues);

    const expectedRoll1 = [5, 4, 5, 3, 6];
    const expectedRoll2 = [6, 6, 4, 6, 6];

    expect(RollHandler.roll([5, 0, 0, 3, 0])).toEqual(expectedRoll1);
    expect(RollHandler.roll([6, 6, 0, 6, 0])).toEqual(expectedRoll2);
  });
});