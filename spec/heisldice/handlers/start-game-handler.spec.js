const td = require('testdouble');
const proxyquire = require('proxyquire');

describe('StartGameHandler', () => {
  let handler;
  let mockDatabaseCommunicator;
  let mockGameFactory;
  let game;

  beforeEach(() => {
    game = {
      players: [],
      gameID: "1234"
    };

    mockDatabaseCommunicator = td.object(['insertGame', 'getCurrentGame']);

    mockGameFactory = td.object(['build']);
    td.when(mockGameFactory.build()).thenReturn(game);

    handler = proxyquire("../../../src/heisldice/handlers/start-game-handler", {
      "../factories/gameFactory": mockGameFactory
    });
  });

  it('should create a new game with an ID and resolve that game\'s ID', () => {
    td.when(mockDatabaseCommunicator.insertGame(game)).thenResolve();
    return handler.execute(mockDatabaseCommunicator).then((result) => {
      expect(result).toEqual(game.gameID);
    });
  });

  it('should create new game until the game ID is unique among active games', async () => {
    const uniqueGame = {
      players: [],
      gameID: "9999"
    };
    const existingGame = {
      players: ["Ryan", "Alex", "Matt"],
      gameID: "1234"
    };
    td.when(mockGameFactory.build()).thenReturn(game, game, uniqueGame);
    td.when(mockDatabaseCommunicator.getCurrentGame(game.gameID)).thenResolve(existingGame);
    td.when(mockDatabaseCommunicator.getCurrentGame(uniqueGame.gameID)).thenResolve(undefined);
    td.when(mockDatabaseCommunicator.insertGame(uniqueGame)).thenResolve();

    const result = await handler.execute(mockDatabaseCommunicator);

    expect(result).toEqual(uniqueGame.gameID);
  });

  describe('should bubble up promise rejection', () => {
    it('from creating a new game', () => {
      const errorToThrow = new Error('hello');
      td.when(mockDatabaseCommunicator.insertGame(game)).thenReject(errorToThrow);

      return handler.execute(mockDatabaseCommunicator).then((result) => {
        fail('returned promise did not reject');
      }).catch((error) => {
        expect(error).toEqual(errorToThrow);
      });
    });
  });
});