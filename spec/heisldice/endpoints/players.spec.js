const proxyquire = require('proxyquire');
const td = require('testdouble');

describe('Players Endpoint', () => {

    let endpoint;
    let mockLogger;
    let request, response, responseWithStatus, next;
    let databaseCommunicator;
    let mockHandler;

    beforeEach(() => {
        mockLogger = td.object(['error']);

        request = {
            body: {
                playerName: 'Peepers',
                diceColor: '#111111'
            },
            params: {
                gameID: '412F',
            },
            game: {
                gameID: '412F'
            }
        };

        response = td.object(['status']);
        responseWithStatus = td.object(['send', 'end']);

        next = td.function('next');

        databaseCommunicator = { "I'm": "a communicumber" };

        mockHandler = td.object(['execute']);

        endpoint = proxyquire('../../../src/heisldice/endpoints/players.js', {
            '../handlers/add-player-handler': mockHandler,
            '../../logger/logger': mockLogger
        });
    });

    describe('PUT', () => {
        it('should respond with player and 200 status if player is present', (done) => {
            const player = { name: "Peepers" };
            td.when(response.status(200)).thenReturn(responseWithStatus);

            request = { player };

            const middleware = endpoint.PUT(databaseCommunicator);
            middleware(request, response, next);

            setTimeout(() => {
                td.verify(responseWithStatus.send(player));
                td.verify(next());
                done();
            });
        });

        it('should add player to game and respond with player and 200 status if player is new', (done) => {
            const player = { name: "Peepers" };
            td.when(mockHandler.execute(request.game, request.body.playerName, databaseCommunicator, request.body.diceColor))
                .thenResolve(player);
            td.when(response.status(200)).thenReturn(responseWithStatus);

            const middleware = endpoint.PUT(databaseCommunicator);
            middleware(request, response, next);

            setTimeout(() => {
                td.verify(responseWithStatus.send(player));
                td.verify(next());
                done();
            });
        });
        it('should return a 500 status', (done) => {
            const err = new Error("harumph");
            td.when(mockHandler.execute(request.game, request.body.playerName, databaseCommunicator, request.body.diceColor))
                .thenReject(err);
            td.when(response.status(500)).thenReturn(responseWithStatus);

            const middleware = endpoint.PUT(databaseCommunicator);
            middleware(request, response, next);

            setTimeout(() => {
                td.verify(mockLogger.error(err));
                td.verify(responseWithStatus.send({ error: err.message }));
                done();
            });
        });
    });
});