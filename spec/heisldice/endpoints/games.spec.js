const proxyquire = require('proxyquire');
const td = require('testdouble');

describe('Games Endpoint', () => {

    let endpoint;
    let mockLogger;
    let request, response, responseWithStatus;
    let databaseCommunicator;
    let mockStartGame;

    beforeEach(() => {
        mockLogger = td.object(['error']);

        request = {
            body: {},
            params: {}
        };

        response = td.object(['status']);
        responseWithStatus = td.object(['send', 'end']);

        databaseCommunicator = { "I'm": "a communicumber" };

        mockStartGame = td.object(['execute']);

        endpoint = proxyquire('../../../src/heisldice/endpoints/games.js', {
            '../handlers/start-game-handler': mockStartGame,
            '../../logger/logger': mockLogger
        });
    });

    describe('POST', () => {
        it('should respond with 201 status', (done) => {
            const gameID = 'K90A'
            td.when(mockStartGame.execute(databaseCommunicator)).thenResolve(gameID);
            td.when(response.status(201)).thenReturn(responseWithStatus);

            const middleware = endpoint.POST(databaseCommunicator);
            middleware(request, response);

            setTimeout(() => {
                td.verify(responseWithStatus.send({ gameID }));
                done();
            });
        });

        it('should return a 500 status', (done) => {
            const error = new Error('mmmmmmmmm');
            td.when(mockStartGame.execute(databaseCommunicator)).thenReject(error);
            td.when(response.status(500)).thenReturn(responseWithStatus);

            const middleware = endpoint.POST(databaseCommunicator);
            middleware(request, response);

            setTimeout(() => {
                td.verify(mockLogger.error(error));
                td.verify(responseWithStatus.send({ error: error.message }));
                done();
            });
        });
    });

    describe('GET', () => {
        beforeEach(() => {
            request = {
                body: {},
                game: {
                    gameID: "893A"
                },
                params: {
                    gameID: "893A",
                }
            };
        });
        it('should respond with game and 200 status', (done) => {
            td.when(response.status(200)).thenReturn(responseWithStatus);

            const middleware = endpoint.GET(databaseCommunicator);
            middleware(request, response);

            setTimeout(() => {
                td.verify(responseWithStatus.send({ game: request.game }));
                done();
            });
        });
    });
});