const proxyquire = require('proxyquire');
const td = require('testdouble');

describe('Dice Endpoint', () => {
    let endpoint;
    let mockLogger;
    let request, response, responseWithStatus;
    let databaseCommunicator;
    let mockDiceRoller, mockRuleChecker, mockRollHandler, mockYahtzeeExceptionChecker;
    let rolledDice, potentialScorecard, player

    beforeEach(() => {
        mockLogger = td.object(['error']);

        request = {
            body: {
                reservedDice: [4, 0, 0, 1, 0]
            },
            params: {
                diceColor: "gameID",
                name: "Ryan"
            },
            game: {
                gameID: "gameID",
            },
            player: {
                name: "Ryan"
            }
        };

        response = td.object(['status']);
        responseWithStatus = td.object(['send', 'end']);

        databaseCommunicator = { "I'm": "a communicumber" };

        mockDiceRoller = td.object(['roll']);
        mockRuleChecker = td.object(['check']);
        mockRollHandler = td.object(['roll']);
        mockYahtzeeExceptionChecker = td.object(['check']);

        rolledDice = [4, 2, 5, 1, 6];
        td.when(mockDiceRoller.roll(request.body.reservedDice)).thenReturn(rolledDice);

        potentialScorecard = {
            "Ames": { score: 4 },
            "Twees": { score: null },
            "fourma kine": { score: 15 }
        };
        td.when(mockRuleChecker.check(rolledDice)).thenReturn(potentialScorecard);

        player = {
            name: "Ryan",
            scorecard: {
                categories: [{ "Shmaces": { score: 5 } }]
            }
        };

        endpoint = proxyquire('../../../src/heisldice/endpoints/dice.js', {
            '../handlers/dice-roller': mockDiceRoller,
            '../handlers/rule-checker': mockRuleChecker,
            '../handlers/player-roll-handler': mockRollHandler,
            '../handlers/yahtzee-exception-checker': mockYahtzeeExceptionChecker,
            '../../logger/logger': mockLogger
        });
    });

    describe('POST', () => {
        it('should roll dice and send data with 200 status', (done) => {
            td.when(mockRollHandler.roll(request.game, request.player, rolledDice, databaseCommunicator)).thenResolve(player);

            const potentialScorecardWithExceptions = {
                "Ames": { score: 4 },
                "Twees": { score: 44 },
                "fourma kine": { score: 15 }
            };
            td.when(mockYahtzeeExceptionChecker.check(potentialScorecard, player.scorecard, rolledDice)).thenReturn(potentialScorecardWithExceptions);

            td.when(response.status(200)).thenReturn(responseWithStatus);

            const middleware = endpoint.POST(databaseCommunicator);
            const next = td.function('next');
            middleware(request, response, next);

            setTimeout(() => {
                td.verify(responseWithStatus.send({
                    dice: rolledDice,
                    scorecard: potentialScorecardWithExceptions,
                    player: player
                }));
                td.verify(next());
                done();
            });
        });

        it('should log error and return a 500 status for other problems', (done) => {
            const error = new Error("something went WRANG");
            td.when(mockRollHandler.roll(request.game, request.player, rolledDice, databaseCommunicator)).thenReject(error);
            td.when(response.status(500)).thenReturn(responseWithStatus);

            const middleware = endpoint.POST(databaseCommunicator);
            middleware(request, response);

            setTimeout(() => {
                td.verify(mockLogger.error(error));
                td.verify(responseWithStatus.send({ error: error.message }));
                done();
            });
        });
    });
});