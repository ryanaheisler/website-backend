const proxyquire = require('proxyquire');
const td = require('testdouble');

describe('Colors Endpoint', () => {
    let endpoint;
    let databaseCommunicator;
    let mockColorChangeHandler;
    let mockLogger;
    let request, response, responseWithStatus;
    let color;


    beforeEach(() => {
        color = "#f02abb"

        databaseCommunicator = { database: "comm" };

        request = {
            params: {
                diceColor: color,
                name: "Ryan"
            },
            game: {
                gameID: "gameID",
            },
            player: {
                name: "Ryan"
            }
        };

        response = td.object(['status']);
        responseWithStatus = td.object(['send', 'end']);

        mockColorChangeHandler = td.object(['setColor']);
        mockLogger = td.object(['error']);

        endpoint = proxyquire('../../../src/heisldice/endpoints/colors.js', {
            '../handlers/change-dice-color-handler': mockColorChangeHandler,
            '../../logger/logger': mockLogger
        });
    });

    it('should call the color change handler and return the correct response', (done) => {
        td.when(response.status(200)).thenReturn(responseWithStatus);
        td.when(mockColorChangeHandler.setColor(request.game, request.player, color, databaseCommunicator))
            .thenResolve(request.player);

        const middleware = endpoint.PUT(databaseCommunicator);
        const next = td.function('next');
        middleware(request, response, next);

        setTimeout(() => {
            td.verify(responseWithStatus.send({ player: request.player }));
            td.verify(next());
            done();
        });
    });

    it('should log error and return a 500 status for any problems', (done) => {
        const error = new Error("something went WRANG");
        td.when(mockColorChangeHandler.setColor(request.game, request.player, color, databaseCommunicator))
            .thenReject(error);
        td.when(response.status(500)).thenReturn(responseWithStatus);

        const middleware = endpoint.PUT(databaseCommunicator);
        middleware(request, response);

        setTimeout(() => {
            td.verify(mockLogger.error(error));
            td.verify(responseWithStatus.send({ error: error.message }));
            done();
        });
    });
});