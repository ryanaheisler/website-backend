const proxyquire = require('proxyquire');
const td = require('testdouble');

describe('score Endpoint', () => {

    let endpoint;
    let mockLogger;
    let request, response, responseWithStatus, next;
    let databaseCommunicator;
    let mockHandler;

    beforeEach(() => {
        mockLogger = td.object(['error']);

        request = {
            body: {
                category: 'shmattergory',
                dice: [1, 2, 3, 4, 5]
            },
            params: {
                gameID: '412F',
                name: 'Crampush',
            },
            game: {
                gameID: '412F',
            },
            player: {
                name: 'Crampush'
            }
        };

        response = td.object(['status']);
        responseWithStatus = td.object(['send', 'end']);

        next = td.function('next');

        databaseCommunicator = { "I'm": "a communicumber" };

        mockHandler = td.object(['execute']);

        endpoint = proxyquire('../../../src/heisldice/endpoints/score.js', {
            '../handlers/set-score-handler': mockHandler,
            '../../logger/logger': mockLogger
        });
    });

    describe('POST', () => {
        it('should respond with player and 200 status', (done) => {
            const player = { name: "scramples" };
            td.when(mockHandler.execute(request.game, request.player, request.body.category, request.body.dice, databaseCommunicator))
                .thenResolve(player);
            td.when(response.status(200)).thenReturn(responseWithStatus);

            const middleware = endpoint.POST(databaseCommunicator);
            middleware(request, response, next);

            setTimeout(() => {
                td.verify(responseWithStatus.send(player));
                td.verify(next());
                done();
            });
        });

        it('should return a 500 status', (done) => {
            const err = new Error("thbttt");
            td.when(mockHandler.execute(request.game, request.player, request.body.category, request.body.dice, databaseCommunicator))
                .thenReject(err);
            td.when(response.status(500)).thenReturn(responseWithStatus);

            const middleware = endpoint.POST(databaseCommunicator);
            middleware(request, response, next);

            setTimeout(() => {
                td.verify(mockLogger.error(err));
                td.verify(responseWithStatus.send({ error: err.message }));
                done();
            });
        });
    });
});