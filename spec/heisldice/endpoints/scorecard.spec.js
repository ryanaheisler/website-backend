const proxyquire = require('proxyquire');
const td = require('testdouble');

describe('Scorecard Endpoint', () => {

    let endpoint;
    let databaseCommunicator;
    let mockLogger;
    let request, response, responseWithStatus;
    let mockHandler;

    beforeEach(() => {
        mockLogger = td.object(['error']);

        request = {
            body: {},
            params: {}
        };

        response = td.object(['status']);
        responseWithStatus = td.object(['send', 'end']);

        databaseCommunicator = { "I'm": "a communicumber" };

        mockHandler = td.object(['getScorecard']);

        endpoint = proxyquire('../../../src/heisldice/endpoints/scorecard.js', {
            '../handlers/blank-scorecard-handler': mockHandler,
            '../../logger/logger': mockLogger
        });
    });
    describe('GET', () => {
        it('should send a blank scorecard with 200 status', () => {
            const scorecard = { score: 1 };
            td.when(mockHandler.getScorecard()).thenReturn(scorecard)
            td.when(response.status(200)).thenReturn(responseWithStatus);

            const middleware = endpoint.GET(databaseCommunicator);
            middleware(request, response);

            td.verify(responseWithStatus.send(scorecard));
        });
    });
});