const td = require('testdouble');
const proxyquire = require('proxyquire');

describe('Dominoes Router', () => {
    let mockExpress;
    let mockRouter;
    let mockDatabaseCommunicator;
    let lobbiesGet, lobbiesPost
    let lobbiesEndpoints
    let lobbyExtractorMiddleware;

    let heisldiceRoutes;
    beforeEach(() => {
        mockRouter = td.object(['get', 'post', 'put']);

        mockExpress = td.object('Router');
        td.when(mockExpress.Router()).thenReturn(mockRouter);

        mockDatabaseCommunicator = { 'database': 'comm' };

        lobbiesPost = { name: "lobbiesPost" };
        lobbiesGet = { name: "lobbiesGet" };
        lobbiesEndpoints = td.object(['POST', 'GET']);
        td.when(lobbiesEndpoints.POST(mockDatabaseCommunicator)).thenReturn(lobbiesPost);
        td.when(lobbiesEndpoints.GET(mockDatabaseCommunicator)).thenReturn(lobbiesGet);

        lobbyExtractorMiddleware = { lobbyExtractor: "middleware" };
        const mockLobbyExtractor = td.function('lobbyExtractor');
        td.when(mockLobbyExtractor(mockDatabaseCommunicator)).thenReturn(lobbyExtractorMiddleware);

        heisldiceRoutes = proxyquire('../../src/routers/dominoes-routes', {
            'express': mockExpress,
            '../dominoes/endpoints/lobbies': lobbiesEndpoints,
            '../dominoes/middleware/lobby-extractor': mockLobbyExtractor,
        });
    });
    it('should create a router with the dominoes endpoints and return it', () => {
        const router = heisldiceRoutes.getRouter(mockDatabaseCommunicator);

        td.verify(mockRouter.post('/lobbies', lobbiesPost));
        td.verify(mockRouter.get('/lobbies/:lobbyID', lobbyExtractorMiddleware, lobbiesGet));

        expect(router).toBe(mockRouter);
    });
});