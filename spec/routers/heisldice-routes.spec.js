const td = require('testdouble');
const proxyquire = require('proxyquire');

describe('Heisldice Router', () => {
    let mockExpress;
    let mockRouter;
    let mockDatabaseCommunicator;
    let gamesGet, gamesPost, playersPut, dicePost, scorePost, scorecardGet, diceColorPut;
    let gamesEndpoints, playersEndpoints, diceEndpoints, scoreEndpoints, scorecardEndpoints, diceColorEndpoints;

    let gameExtractorMiddleware;
    let playerParamsExtractorMiddleware;
    let playerBodyExtractorMiddleware;

    let isItMyTurnMiddleware;
    let broadcastGameMiddleware;

    let heisldiceRoutes;
    beforeEach(() => {
        mockRouter = td.object(['get', 'post', 'put']);

        mockExpress = td.object('Router');
        td.when(mockExpress.Router()).thenReturn(mockRouter);

        mockDatabaseCommunicator = { 'database': 'comm' };

        gamesGet = { name: "gamesGet" };
        gamesPost = { name: "gamesPost" };
        gamesEndpoints = td.object(['GET', 'POST']);
        td.when(gamesEndpoints.GET(mockDatabaseCommunicator)).thenReturn(gamesGet);
        td.when(gamesEndpoints.POST(mockDatabaseCommunicator)).thenReturn(gamesPost);

        playersPut = { name: "playersPut" };
        playersEndpoints = td.object(['PUT']);
        td.when(playersEndpoints.PUT(mockDatabaseCommunicator)).thenReturn(playersPut);

        dicePost = { name: "dicePost" };
        diceEndpoints = td.object(['POST']);
        td.when(diceEndpoints.POST(mockDatabaseCommunicator)).thenReturn(dicePost);

        scorePost = { name: "scorePost" };
        scoreEndpoints = td.object(['POST']);
        td.when(scoreEndpoints.POST(mockDatabaseCommunicator)).thenReturn(scorePost);

        scorecardGet = { name: "scorecardGet" };
        scorecardEndpoints = td.object(['GET']);
        td.when(scorecardEndpoints.GET(mockDatabaseCommunicator)).thenReturn(scorecardGet);

        diceColorPut = { name: "diceColorPut" };
        diceColorEndpoints = td.object(['PUT']);
        td.when(diceColorEndpoints.PUT(mockDatabaseCommunicator)).thenReturn(diceColorPut);

        gameExtractorMiddleware = { gameExtractor: "middleware" };
        const mockGameExtractor = td.function('gameExtractor');
        td.when(mockGameExtractor(mockDatabaseCommunicator)).thenReturn(gameExtractorMiddleware);

        playerBodyExtractorMiddleware = { playerExtractor: "body" };
        const mockPlayerBodyExtractor = td.function('playerExtractor');
        td.when(mockPlayerBodyExtractor()).thenReturn(playerBodyExtractorMiddleware);

        playerParamsExtractorMiddleware = { playerExtractor: "params" };
        const mockPlayerParamsExtractor = td.function('playerExtractor');
        td.when(mockPlayerParamsExtractor()).thenReturn(playerParamsExtractorMiddleware);

        broadcastGameMiddleware = { broadcast: "middleware" };
        const mockBroadcastGameGetter = td.function('gameBroadcaster');
        td.when(mockBroadcastGameGetter(mockDatabaseCommunicator)).thenReturn(broadcastGameMiddleware);

        isItMyTurnMiddleware = { myTurn: "my turn?" };
        const mockIsItMyTurn = td.function('isItMyTurn');
        td.when(mockIsItMyTurn(mockDatabaseCommunicator)).thenReturn(isItMyTurnMiddleware);

        heisldiceRoutes = proxyquire('../../src/routers/heisldice-routes', {
            'express': mockExpress,
            '../heisldice/endpoints/games': gamesEndpoints,
            '../heisldice/endpoints/players': playersEndpoints,
            '../heisldice/endpoints/dice': diceEndpoints,
            '../heisldice/endpoints/score': scoreEndpoints,
            '../heisldice/endpoints/scorecard': scorecardEndpoints,
            '../heisldice/endpoints/colors': diceColorEndpoints,
            '../heisldice/middleware/game-extractor': mockGameExtractor,
            '../heisldice/middleware/player-body-extractor': mockPlayerBodyExtractor,
            '../heisldice/middleware/player-param-extractor': mockPlayerParamsExtractor,
            '../heisldice/middleware/game_broadcaster': mockBroadcastGameGetter,
            '../heisldice/middleware/is-it-my-turn': mockIsItMyTurn,
        });
    });
    it('should create a router with the heisldice endpoints and return it', () => {
        const router = heisldiceRoutes.getRouter(mockDatabaseCommunicator);

        td.verify(mockRouter.post('/games', gamesPost));
        td.verify(mockRouter.get('/games/:gameID', gameExtractorMiddleware, gamesGet));

        td.verify(mockRouter.put(
            '/games/:gameID/players',
            gameExtractorMiddleware,
            playerBodyExtractorMiddleware,
            playersPut,
            broadcastGameMiddleware
        ));

        td.verify(mockRouter.post(
            '/games/:gameID/players/:name/dice',
            gameExtractorMiddleware,
            playerParamsExtractorMiddleware,
            isItMyTurnMiddleware,
            dicePost,
            broadcastGameMiddleware));

        td.verify(mockRouter.post(
            '/games/:gameID/players/:name/score',
            gameExtractorMiddleware,
            playerParamsExtractorMiddleware,
            isItMyTurnMiddleware,
            scorePost,
            broadcastGameMiddleware));

        td.verify(mockRouter.put(
            '/games/:gameID/players/:name/dice-color/:diceColor',
            gameExtractorMiddleware,
            playerParamsExtractorMiddleware,
            diceColorPut,
            broadcastGameMiddleware
        ))

        td.verify(mockRouter.get('/scorecard', scorecardGet));

        expect(router).toBe(mockRouter);
    });
});