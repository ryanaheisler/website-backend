const configuration = require('../configuration.js');

describe('configuration', () => {
  describe("from ENV", () => {
    beforeEach(() => {
      process.env.HEISLSITE_PORT = 6666;
      process.env.HEISLSITE_MONGODB_URI = 'Its here';
      process.env.HEISLSITE_MONGODB_NAME = 'Mango';
      process.env.HEISLSITE_MONGODB_COLLECTION_PLAYERS = 'Playzees';
      process.env.HEISLSITE_MONGODB_COLLECTION_GAMES = 'James';
      process.env.HEISLSITE_MONGODB_COLLECTION_DOMINOES_LOBBIES = 'Bobbies!';
    });

    it('should have correct values', () => {
      expect(configuration.getCorsWhitelist()).toEqual([
        "http://localhost:4200",
        "http://www.ryanheisler.com",
        "http://ryanheisler.com",
        "http://rheisler-website.herokuapp.com"
      ]);
      expect(configuration.getPort()).toEqual('6666');
      expect(configuration.getMongoURI()).toEqual('Its here');
      expect(configuration.getMongoDBName()).toEqual('Mango');
      expect(configuration.getMongoPlayersCollection()).toEqual('Playzees');
      expect(configuration.getMongoGamesCollection()).toEqual('James');
      expect(configuration.getMongoDominoesLobbies()).toEqual('Bobbies!');
    });
  });
  describe('Defaults', () => {
    beforeEach(() => {
      delete process.env.HEISLSITE_PORT;
      delete process.env.HEISLSITE_MONGODB_URI;
      delete process.env.HEISLSITE_MONGODB_NAME;
      delete process.env.HEISLSITE_MONGODB_COLLECTION_PLAYERS;
      delete process.env.HEISLSITE_MONGODB_COLLECTION_GAMES;
      delete process.env.HEISLSITE_MONGODB_COLLECTION_DOMINOES_LOBBIES;
    });
    it('should have the correct defaults', () => {
      expect(configuration.getPort()).toEqual('3000');
      expect(configuration.getMongoURI()).toEqual('mongodb://localhost:27017/');
      expect(configuration.getMongoDBName()).toEqual('heislsite');
      expect(configuration.getMongoPlayersCollection()).toEqual('players');
      expect(configuration.getMongoGamesCollection()).toEqual('games');
      expect(configuration.getMongoDominoesLobbies()).toEqual('dominoes_lobbies');
    });
  });
});