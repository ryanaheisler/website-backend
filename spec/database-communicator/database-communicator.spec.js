const proxyquire = require('proxyquire');
const td = require('testdouble');
const mongo = require('mongodb');
const config = require('../../configuration.js')

describe('DatabaseCommunicator', () => {

  let DatabaseCommunicator;
  let mockMongoClientConstructor;
  let mockMongoClient;
  let mockMongoClientFromPromise;
  let communicator;
  let mockMongoDB;
  let mockPlayersCollection;
  let mockGamesCollection;
  let mockDominoesLobbiesCollection;

  beforeEach(() => {
    mockMongoClientConstructor = td.function();
    const mockMongodb = {
      MongoClient: mockMongoClientConstructor
    };

    mockMongoClient = td.object(['connect']);
    td.when(mockMongoClientConstructor(config.getMongoURI())).thenReturn(mockMongoClient);

    mockMongoClientFromPromise = td.object(['db', 'close']);
    td.when(mockMongoClient.connect()).thenResolve(mockMongoClientFromPromise);

    mockMongoDB = td.object(['collection']);
    td.when(mockMongoClientFromPromise.db(config.getMongoDBName())).thenReturn(mockMongoDB);

    mockPlayersCollection = td.object(['find', 'insertOne']);
    td.when(mockMongoDB.collection(config.getMongoPlayersCollection())).thenReturn(mockPlayersCollection);

    mockGamesCollection = td.object(['find', 'insertOne', 'update']);
    td.when(mockMongoDB.collection(config.getMongoGamesCollection())).thenReturn(mockGamesCollection);

    mockDominoesLobbiesCollection = td.object(['find', 'insertOne', 'update']);
    td.when(mockMongoDB.collection(config.getMongoDominoesLobbies())).thenReturn(mockDominoesLobbiesCollection);

    DatabaseCommunicator = proxyquire("../../src/database-communicator/database-communicator", {
      'mongodb': mockMongodb
    });

    communicator = new DatabaseCommunicator();
  });

  afterEach(() => {
    communicator.close();
    td.verify(mockMongoClientFromPromise.close());
  });

  describe('game', () => {
    it('should insert a new game', (done) => {
      const game = {};
      const expectedPromise = new Promise(() => { });
      td.when(mockGamesCollection.insertOne(game)).thenReturn(expectedPromise);

      communicator.whenReady().then((communicator) => {
        promise = communicator.insertGame(game);
        expect(promise).toBe(expectedPromise);
        done();
      });
    });

    it('should get the currently in-progress game by gameID', () => {
      const gameID = "555534"
      const mockCursor = td.object(['limit']);
      const mockCursor2 = td.object(['next']);
      td.when(mockGamesCollection.find({ gameID: gameID })).thenReturn(mockCursor);

      const expectedPromise = new Promise(() => { });

      const currentGame = {};
      td.when(mockCursor.limit(1)).thenReturn(mockCursor2);
      td.when(mockCursor2.next()).thenReturn(expectedPromise);

      return communicator.whenReady().then((communicator) => {
        const promise = communicator.getCurrentGame(gameID);
        expect(promise).toBe(expectedPromise);
      });
    });

    it('should update an existing game', () => {
      const criteria = { _id: 123 };
      const updatedGame = { _id: 123, players: [] };
      const expectedPromise = new Promise(() => { });
      td.when(mockGamesCollection.update(criteria, updatedGame)).thenReturn(expectedPromise);

      return communicator.whenReady().then((communicator) => {
        const promise = communicator.updateGame(updatedGame);
        expect(promise).toBe(expectedPromise);
      });
    });
  });

  describe('dominoes lobby', () => {
    it('should insert a new lobby', (done) => {
      const lobby = {};
      const expectedPromise = new Promise(() => { });
      td.when(mockDominoesLobbiesCollection.insertOne(lobby)).thenReturn(expectedPromise);

      communicator.whenReady().then((communicator) => {
        promise = communicator.insertDominoesLobby(lobby);
        expect(promise).toBe(expectedPromise);
        done();
      });
    });

    it('should get the currently in-progress lobby by ID', () => {
      const lobbyID = "555534"
      const mockCursor = td.object(['limit']);
      const mockCursor2 = td.object(['next']);
      td.when(mockDominoesLobbiesCollection.find({ lobbyID: lobbyID })).thenReturn(mockCursor);

      const expectedPromise = new Promise(() => { });

      const currentlobby = {};
      td.when(mockCursor.limit(1)).thenReturn(mockCursor2);
      td.when(mockCursor2.next()).thenReturn(expectedPromise);

      return communicator.whenReady().then((communicator) => {
        const promise = communicator.getCurrentDominoesLobby(lobbyID);
        expect(promise).toBe(expectedPromise);
      });
    });

    it('should update an existing lobby', () => {
      const criteria = { _id: 123 };
      const updatedLobby = { _id: 123 };
      const expectedPromise = new Promise(() => { });
      td.when(mockDominoesLobbiesCollection.update(criteria, updatedLobby)).thenReturn(expectedPromise);

      return communicator.whenReady().then((communicator) => {
        const promise = communicator.updateDominoesLobby(updatedLobby);
        expect(promise).toBe(expectedPromise);
      });
    });
  });
});