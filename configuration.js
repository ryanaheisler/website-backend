const config = require('./configuration.json');

class Configuration {
  static getCorsWhitelist() {
    return config.corsWhitelist;
  }
  static getPort() {
    return process.env.HEISLSITE_PORT || "3000";
  }
  static getMongoURI() {
    return process.env.HEISLSITE_MONGODB_URI || "mongodb://localhost:27017/";
  }
  static getMongoDBName() {
    return process.env.HEISLSITE_MONGODB_NAME || "heislsite";
  }
  static getMongoPlayersCollection() {
    return process.env.HEISLSITE_MONGODB_COLLECTION_PLAYERS || "players";
  }
  static getMongoGamesCollection() {
    return process.env.HEISLSITE_MONGODB_COLLECTION_GAMES || "games";
  }
  static getMongoDominoesLobbies() {
    return process.env.HEISLSITE_MONGODB_COLLECTION_DOMINOES_LOBBIES || "dominoes_lobbies";
  }
}

module.exports = Configuration;