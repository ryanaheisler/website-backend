const lobbyFactory = require('../factories/dominoes-lobby-factory');

class StartLobbyHandler {
  static async execute(databaseCommunicator) {
    let lobby = await getUniqueLobby(databaseCommunicator);
    await databaseCommunicator.insertDominoesLobby(lobby)
    return lobby.lobbyID;
  }
}

async function getUniqueLobby(databaseCommunicator) {
  let lobby = lobbyFactory.build();
  let currentLobby = await databaseCommunicator.getCurrentDominoesLobby(lobby.lobbyID);
  if (currentLobby && currentLobby.lobbyID === lobby.lobbyID) {
      return getUniqueLobby(databaseCommunicator);
  }
  return lobby;
}

module.exports = StartLobbyHandler;