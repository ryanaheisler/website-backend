const constants = require('../constants');

module.exports = (databaseCommunicator) => {
  return async (request, response, next) => {
    const lobby = await databaseCommunicator.getCurrentDominoesLobby(request.params.lobbyID);
    if (lobby) {
      request.lobby = lobby;
      next();
    } else {
      response.status(404).send({ error: constants.ERROR_MESSAGES.LOBBY_NOT_FOUND(request.params.lobbyID) });
    }
  }
}