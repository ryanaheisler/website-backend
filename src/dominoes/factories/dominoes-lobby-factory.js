const idGenerator = require('../../helpers/game-id-generator');

const LobbyFactory = {
  build: () => {
    return {
      players: [],
      lobbyID: idGenerator.getID(),
      startTime: Date.now()
    }
  }

}
module.exports = LobbyFactory