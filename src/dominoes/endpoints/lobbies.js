const startLobby = require('../handlers/start-lobby-handler');
const logger = require('../../logger/logger');

module.exports = {
    POST: (databaseCommunicator) => {
        return (request, response) => {
            startLobby.execute(databaseCommunicator).then((lobbyID) => {
                response.status(201).send({ lobbyID: lobbyID });
            }).catch((error) => {
                logger.error(error);
                response.status(500).send({ error: error.message });
            });
        }
    },
    GET: (databaseCommunicator) => {
        return (request, response) => {
            response.status(200).send({ lobby: request.lobby });
        }
    }
}