module.exports = {
  ACES: "Aces",
  TWOS: "Twos",
  THREES: "Threes",
  FOURS: "Fours",
  FIVES: "Fives",
  SIXES: "Sixes",
  THREE_OF_A_KIND: "3 of a Kind",
  FOUR_OF_A_KIND: "4 of a Kind",
  FULL_HOUSE: "Full House",
  SMALL_STRAIGHT: "Small Straight",
  LARGE_STRAIGHT: "Large Straight",
  YAHTZEE: "Yahtzee",
  CHANCE: "Chance"
}