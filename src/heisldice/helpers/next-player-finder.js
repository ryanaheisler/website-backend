module.exports = (game) => {
  const playersInTurnOrder = [...game.players].sort((a, b) => a.index - b.index);

  while (!playersInTurnOrder[0].myTurn) {
    playersInTurnOrder.push(playersInTurnOrder.shift());
  }
  playersInTurnOrder.push(playersInTurnOrder.shift());

  const mostTurnsRemaining = Math.max(...playersInTurnOrder.map(player => player.turnsRemaining));

  for (let i = 0; i < playersInTurnOrder.length; i++) {
    const player = playersInTurnOrder[i];

    if (player.turnsRemaining === mostTurnsRemaining) return player;
  }
}