const ruleChecker = require('../handlers/rule-checker');
const cats = require('../categories');

module.exports = {
  getUpdatedScorecard: (scorecard, categoryDescription, dice) => {
    const newScorecard = JSON.parse(JSON.stringify(scorecard));
    const score = ruleChecker.checkSingleRule(categoryDescription, dice);

    newScorecard.categories[categoryDescription].score = score;

    topSectionTotal = getTotalForSection(newScorecard, topSection);
    topSectionBonus = topSectionTotal >= 63 ? 35 : 0

    newScorecard.topSectionBonus = topSectionBonus;
    newScorecard.topSectionTotal = topSectionTotal + topSectionBonus;

    newScorecard.bottomSectionTotal = getTotalForSection(newScorecard, bottomSection);

    const sortedDice = dice.sort();
    const yahtzee = sortedDice[0] === sortedDice[sortedDice.length - 1];
    const zeroInYahtzee = newScorecard.categories[cats.YAHTZEE].score === 0;
    const yahtzeeAlreadyFilled = newScorecard.categories[cats.YAHTZEE].score != null;
    const firstYahtzeeThisTurn = !(categoryDescription === cats.YAHTZEE);
    if (yahtzee && yahtzeeAlreadyFilled && !zeroInYahtzee && firstYahtzeeThisTurn) {
      newScorecard.extraYahtzeeBonusTotal += 100;
    }
    newScorecard.bottomSectionTotal += newScorecard.extraYahtzeeBonusTotal;

    newScorecard.total = newScorecard.topSectionTotal + newScorecard.bottomSectionTotal;

    return newScorecard;
  }
}

function getTotalForSection(scorecard, section) {
  let total = 0;
  section.forEach(category => {
    total += scorecard.categories[category].score;
  });
  return total;
}
const topSection = [cats.ACES, cats.TWOS, cats.THREES, cats.FOURS, cats.FIVES, cats.SIXES]
const bottomSection = [cats.THREE_OF_A_KIND, cats.FOUR_OF_A_KIND, cats.FULL_HOUSE, cats.SMALL_STRAIGHT,
cats.LARGE_STRAIGHT, cats.YAHTZEE, cats.CHANCE]