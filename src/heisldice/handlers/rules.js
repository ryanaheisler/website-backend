const makeNumericReducer = (numeric) => {
  const reducer = (accumulator, currentValue) => {
    return accumulator + (currentValue == numeric ? currentValue : 0);
  };

  return reducer
}

const sumOfAll = (dice) => {
  return dice.reduce((accumulator, currentValue) => {
    return accumulator += currentValue;
  })
}

const getCountOfAscendingPairs = (sortedDice) => {
  let count = 0;
  for (let index = 1; index < sortedDice.length; index++) {
    const pair = [sortedDice[index - 1], sortedDice[index]];
    if (pair[1] - pair[0] === 1) {
      count++;
    }
    else if (pair[1] - pair[0] !== 0) {
      if (index !== sortedDice.length - 1) {
        count = 0;
      }
    }
  }
  return count;
}

const rules = {
  aces: (dice) => {
    return dice.reduce(makeNumericReducer(1), 0);
  },
  twos: (dice) => {
    return dice.reduce(makeNumericReducer(2), 0);
  },
  threes: (dice) => {
    return dice.reduce(makeNumericReducer(3), 0);
  },
  fours: (dice) => {
    return dice.reduce(makeNumericReducer(4), 0);
  },
  fives: (dice) => {
    return dice.reduce(makeNumericReducer(5), 0);
  },
  sixes: (dice) => {
    return dice.reduce(makeNumericReducer(6), 0);
  },
  threeOfAKind: (dice) => {
    let count = [0, 0, 0, 0, 0, 0];
    dice.forEach(die => {
      count[die - 1] += 1
    });
    return count.findIndex(value => value >= 3) == -1 ? 0 : sumOfAll(dice);
  },
  fourOfAKind: (dice) => {
    let count = [0, 0, 0, 0, 0, 0];
    dice.forEach(die => {
      count[die - 1] += 1
    });
    return count.findIndex(value => value >= 4) == -1 ? 0 : sumOfAll(dice);
  },
  fullHouse: (dice) => {
    let count = [0, 0, 0, 0, 0, 0];
    dice.forEach(die => {
      count[die - 1] += 1
    });
    const shouldScore = (count.includes(3) && count.includes(2)) || count.includes(5);
    return shouldScore ? 25 : 0;
  },
  smallStraight: (dice) => {
    const sortedDice = [...dice].sort();
    count = getCountOfAscendingPairs(sortedDice);
    return count >= 3 ? 30 : 0;
  },
  largeStraight: (dice) => {
    const sortedDice = [...dice].sort();
    count = getCountOfAscendingPairs(sortedDice);
    return count >= 4 ? 40 : 0;
  },
  yahtzee: (dice) => {
    let count = [0, 0, 0, 0, 0, 0];
    dice.forEach(die => {
      count[die - 1] += 1
    });
    return count.includes(5) ? 50 : 0;
  },
  chance: (dice) => {
    return sumOfAll(dice);
  }
}

module.exports = rules;