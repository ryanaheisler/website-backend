module.exports = {
  roll: (reservedDice) => {
    const dice = [...reservedDice];
    for (let index = 0; index < 5; index++) {
      const randomNumber = Math.random();
      if (dice[index] === 0) {
        dice[index] = getDiceFromRandom(randomNumber);
      }
    }
    return dice;
  }
}

const getDiceFromRandom = (value) => {
  return value > 0 ? Math.ceil(value * 6) : 1;
}