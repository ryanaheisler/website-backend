const playerFactory = require('../factories/playerFactory');
const constants = require('../constants');

module.exports = {
  execute: async (game, playerName, databaseCommunicator, diceColor) => {
    const builtPlayer = playerFactory.buildNewPlayer(playerName, game.players.length, diceColor);
    const indexOfPlayerWithName = game.players.findIndex((player) => player.name === playerName);
    if (indexOfPlayerWithName !== -1) {
      return Promise.reject(new Error(constants.ERROR_MESSAGES.PLAYER_NAME_ALREADY_USED(playerName)));
    }

    game.players.push(builtPlayer);
    await databaseCommunicator.updateGame(game)
    return builtPlayer;
  }
}