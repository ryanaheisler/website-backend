const gameFactory = require('../factories/gameFactory');

class StartGameHandler {
  static async execute(databaseCommunicator) {
    let game = await getUniqueGame(databaseCommunicator);
    await databaseCommunicator.insertGame(game)
    return game.gameID;
  }
}

async function getUniqueGame(databaseCommunicator) {
  let game = gameFactory.build();
  let currentGame = await databaseCommunicator.getCurrentGame(game.gameID);
  if (currentGame && currentGame.gameID === game.gameID) {
      return getUniqueGame(databaseCommunicator);
  }
  return game;
}

module.exports = StartGameHandler;