const cats = require('../categories')

module.exports = {
  getScorecard: () => {

    const scorecard = {
      topSectionBonus: 0,
      topSectionTotal: 0,
      extraYahtzeeBonusTotal: 0,
      bottomSectionTotal: 0,
      total: 0,
      categories: {}
    }
    scorecard.categories[cats.ACES] = {
      score: null,
      section: 0
    };
    scorecard.categories[cats.TWOS] = {
      score: null,
      section: 0
    };
    scorecard.categories[cats.THREES] = {
      score: null,
      section: 0
    };
    scorecard.categories[cats.FOURS] = {
      score: null,
      section: 0
    };
    scorecard.categories[cats.FIVES] = {
      score: null,
      section: 0
    };
    scorecard.categories[cats.SIXES] = {
      score: null,
      section: 0
    };
    scorecard.categories[cats.THREE_OF_A_KIND] = {
      score: null,
      section: 1
    };
    scorecard.categories[cats.FOUR_OF_A_KIND] = {
      score: null,
      section: 1
    };
    scorecard.categories[cats.FULL_HOUSE] = {
      score: null,
      section: 1
    };
    scorecard.categories[cats.SMALL_STRAIGHT] = {
      score: null,
      section: 1
    };
    scorecard.categories[cats.LARGE_STRAIGHT] = {
      score: null,
      section: 1
    };
    scorecard.categories[cats.YAHTZEE] = {
      score: null,
      section: 1
    };
    scorecard.categories[cats.CHANCE] = {
      score: null,
      section: 1
    }

    return scorecard;
  }
}