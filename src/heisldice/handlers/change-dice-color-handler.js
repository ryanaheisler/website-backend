module.exports = {
    setColor: (game, player, color, databaseCommunicator) => {
        player.diceColor = color;
        return databaseCommunicator.updateGame(game).then(() => {
            return player;
        });
    }
}