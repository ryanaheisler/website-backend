const constants = require('../constants');

module.exports = {
  roll: (game, currentPlayer, rolledDice, databaseCommunicator) => {
    currentPlayer.rollsRemaining -= 1;
    currentPlayer.previousDice = rolledDice;
    return databaseCommunicator.updateGame(game).then(() => {
      return currentPlayer;
    });
  }
}