const cats = require('../categories');

module.exports = {
  check: (potentialScorecard, actualScorecard, dice) => {
    const newPotentialScorecard = JSON.parse(JSON.stringify(potentialScorecard));

    if (actualScorecard.categories[cats.YAHTZEE].score !== 50) return newPotentialScorecard;

    const sortedDice = dice.sort();
    if (sortedDice[0] !== sortedDice[sortedDice.length - 1]) return newPotentialScorecard;

    const categoryDescription = getCategory(sortedDice);

    const matchingTopSectionScoreNotFilled = actualScorecard.categories[categoryDescription].score === null;

    if (matchingTopSectionScoreNotFilled) {
      const zeroCategories = Object.keys(cats).filter((category) => {
        return cats[category] !== categoryDescription;
      });
      zeroCategories.forEach(category => {
        newPotentialScorecard.categories[cats[category]].score = 0;
      });
    } else {
      newPotentialScorecard.categories[cats.SMALL_STRAIGHT].score = 30;
      newPotentialScorecard.categories[cats.LARGE_STRAIGHT].score = 40;
    }

    return newPotentialScorecard;
  }
}

function getCategory(sortedDice) {
  switch (sortedDice[0]) {
    case 1:
      return cats.ACES;
    case 2:
      return cats.TWOS;
    case 3:
      return cats.THREES;
    case 4:
      return cats.FOURS;
    case 5:
      return cats.FIVES;
    case 6:
      return cats.SIXES;
    default:
      break;
  }
}