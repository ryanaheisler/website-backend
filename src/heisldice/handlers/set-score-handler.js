const scorecardUpdater = require('../helpers/scorecard-helper');
const nextPlayerFinder = require('../helpers/next-player-finder');
const constants = require('../constants');

module.exports = {
  execute: async (currentGame, currentPlayer, categoryDescription, dice, databaseCommunicator) => {
    const updatedScorecard = scorecardUpdater.getUpdatedScorecard(currentPlayer.scorecard, categoryDescription, dice);

    currentPlayer.turnsRemaining -= 1;
    currentPlayer.scorecard = updatedScorecard;
    currentPlayer.rollsRemaining = 3;

    const nextPlayer = nextPlayerFinder(currentGame);

    currentPlayer.myTurn = false;
    nextPlayer.myTurn = true;

    await databaseCommunicator.updateGame(currentGame);

    return currentPlayer;
  }
}