const cats = require('../categories')
const rules = require('./rules')
const scorecardMaker = require('../handlers/blank-scorecard-handler');

module.exports = {

  check: (dice) => {
    const scorecard = scorecardMaker.getScorecard();
    scorecard.categories[cats.ACES].score = rules.aces(dice);
    scorecard.categories[cats.TWOS].score = rules.twos(dice);
    scorecard.categories[cats.THREES].score = rules.threes(dice);
    scorecard.categories[cats.FOURS].score = rules.fours(dice);
    scorecard.categories[cats.FIVES].score = rules.fives(dice);
    scorecard.categories[cats.SIXES].score = rules.sixes(dice);
    scorecard.categories[cats.THREE_OF_A_KIND].score = rules.threeOfAKind(dice);
    scorecard.categories[cats.FOUR_OF_A_KIND].score = rules.fourOfAKind(dice);
    scorecard.categories[cats.FULL_HOUSE].score = rules.fullHouse(dice);
    scorecard.categories[cats.SMALL_STRAIGHT].score = rules.smallStraight(dice);
    scorecard.categories[cats.LARGE_STRAIGHT].score = rules.largeStraight(dice);
    scorecard.categories[cats.YAHTZEE].score = rules.yahtzee(dice);
    scorecard.categories[cats.CHANCE].score = rules.chance(dice);
    return scorecard;
  },

  checkSingleRule: (categoryDescription, dice) => {
    switch (categoryDescription) {
      case cats.ACES:
        return rules.aces(dice);
      case cats.TWOS:
        return rules.twos(dice);
      case cats.THREES:
        return rules.threes(dice);
      case cats.FOURS:
        return rules.fours(dice);
      case cats.FIVES:
        return rules.fives(dice);
      case cats.SIXES:
        return rules.sixes(dice);
      case cats.THREE_OF_A_KIND:
        return rules.threeOfAKind(dice);
      case cats.FOUR_OF_A_KIND:
        return rules.fourOfAKind(dice);
      case cats.FULL_HOUSE:
        return rules.fullHouse(dice);
      case cats.SMALL_STRAIGHT:
        return rules.smallStraight(dice);
      case cats.LARGE_STRAIGHT:
        return rules.largeStraight(dice);
      case cats.YAHTZEE:
        return rules.yahtzee(dice);
      case cats.CHANCE:
        return rules.chance(dice);

      default:
        break;
    }
  }
}