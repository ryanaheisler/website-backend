const constants = require('../constants');

module.exports = () => {
  return async (request, response, next) => {
    const player = request.game.players.find((p) => { return p.name === request.params.name });
    if (player) {
      request.player = player;
      next();
    } else {
      response.status(404).send({ error: constants.ERROR_MESSAGES.PLAYER_NOT_FOUND });
    }
  }
}