const setTimeoutWrapper = require('../../helpers/set-timeout-wrapper');
const SocketMap = require('../../helpers/socket-map');

module.exports = (databaseCommunicator) => {
    const websocketMap = SocketMap.SINGLETON();
    return (request, response) => {
        setTimeoutWrapper(() => {
            const clients = websocketMap.getClients(request.game.gameID);
            console.log(`sending data to ${clients.length} client${clients.length > 1 ? 's' : ''}`);
            clients.forEach(client => {
                client.send(JSON.stringify(request.game));
            });
        }, 100);
    }
}