module.exports = () => {
    return async (request, _response, next) => {
        const player = request.game.players.find((p) => { return p.name === request.body.playerName });
        if (player) {
            request.player = player;
        }
        next();
    }
}