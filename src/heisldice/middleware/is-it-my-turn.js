const constants = require('../constants');

module.exports = (databaseCommunicator) => {
  return async (request, response, next) => {
    const game = await databaseCommunicator.getCurrentGame(request.params.gameID);
    const player = game.players.find((p) => { return p.name === request.params.name });
    if (player.myTurn) {
      next();
    } else {
      response.status(422).send({ error: constants.ERROR_MESSAGES.NOT_YOUR_TURN(player.name) });
    }
  }
}