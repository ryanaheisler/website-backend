const constants = require('../constants');

module.exports = (databaseCommunicator) => {
  return async (request, response, next) => {
    const game = await databaseCommunicator.getCurrentGame(request.params.gameID);
    if (game) {
      request.game = game;
      next();
    } else {
      response.status(404).send({ error: constants.ERROR_MESSAGES.GAME_NOT_FOUND(request.params.gameID) });
    }
  }
}