const categories = require('../categories');
const scorecardMaker = require('../handlers/blank-scorecard-handler');

class PlayerFactory {

  static buildNewPlayer(playerName, index, diceColor) {
    return {
      name: playerName,
      turnsRemaining: Object.keys(categories).length,
      rollsRemaining: 3,
      scorecard: scorecardMaker.getScorecard(),
      previousDice: [1, 2, 3, 4, 5],
      index: index,
      myTurn: index === 0,
      diceColor: diceColor
    };
  }

}

module.exports = PlayerFactory;