const idGenerator = require('../../helpers/game-id-generator');

const GameFactory = {
  build: () => {
    return {
      players: [],
      gameID: idGenerator.getID(),
      startTime: Date.now()
    }
  }

}
module.exports = GameFactory