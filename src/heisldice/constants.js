module.exports = {
  ERROR_MESSAGES: {
    PLAYER_NOT_FOUND: 'The player was not found.',
    GAME_NOT_FOUND: (id) => `Game with ID "${id}" not found.`,
    PLAYER_NAME_ALREADY_USED: (name) => `A player named "${name}" already exists.`,
    NOT_YOUR_TURN: (name) => `It is not ${name}'s turn`
  }
}