const addPlayerHandler = require('../handlers/add-player-handler');
const logger = require('../../logger/logger');

module.exports = {
    PUT: (databaseCommunicator) => {
        return (request, response, next) => {
            if (request.player) {
                response.status(200).send(request.player);
                next();
            } else {

                addPlayerHandler.execute(
                    request.game, request.body.playerName, databaseCommunicator, request.body.diceColor
                )
                    .then((builtPlayer) => {
                        response.status(200).send(builtPlayer);
                        next();
                    })
                    .catch((error) => {
                        logger.error(error);
                        switch (error.message) {
                            default:
                                response = response.status(500);
                                break;
                        }
                        response.send({ error: error.message });
                    });
            }
        }
    }
}