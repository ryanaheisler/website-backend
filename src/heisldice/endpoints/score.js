const setScoreHandler = require('../handlers/set-score-handler');
const logger = require('../../logger/logger');

module.exports = {
    POST: (databaseCommunicator) => {
        return (request, response, next) => {
            setScoreHandler.execute(request.game, request.player, request.body.category, request.body.dice, databaseCommunicator)
                .then((player) => {
                    response.status(200).send(player);
                    next();
                })
                .catch((error) => {
                    response = response.status(500);
                    logger.error(error);
                    response.send({ error: error.message });
                });
        }
    }
}