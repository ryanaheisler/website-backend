const handler = require("../handlers/change-dice-color-handler");
const logger = require('../../logger/logger');

module.exports = {
    PUT: (databaseCommunicator) => {
        const endpoint = async (request, response, next) => {
            try {
                const updatedPlayer = await handler.setColor(
                    request.game, request.player, request.params.diceColor, databaseCommunicator
                )
                response.status(200).send({ player: updatedPlayer });
                next();
            } catch (e) {
                logger.error(e);
                response.status(500).send({ error: e.message });
            }
        }

        return endpoint;
    }
}