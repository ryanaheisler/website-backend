const startGame = require('../handlers/start-game-handler');
const logger = require('../../logger/logger');

module.exports = {
    POST: (databaseCommunicator) => {
        return (request, response) => {
            startGame.execute(databaseCommunicator).then((gameID) => {
                response.status(201).send({ gameID: gameID });
            }).catch((error) => {
                logger.error(error);
                response.status(500).send({ error: error.message });
            });
        }
    },
    GET: (databaseCommunicator) => {
        return (request, response) => {
            response.status(200).send({ game: request.game });
        }
    }
}