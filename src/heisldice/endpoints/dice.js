const diceRoller = require('../handlers/dice-roller');
const ruleChecker = require('../handlers/rule-checker');
const playerRollHandler = require('../handlers/player-roll-handler');
const yahtzeeExceptionChecker = require('../handlers/yahtzee-exception-checker');
const logger = require('../../logger/logger');

module.exports = {
    POST: (databaseCommunicator) => {
        const endpoint = (request, response, next) => {
            const diceValues = diceRoller.roll(request.body.reservedDice);
            const potentialScorecard = ruleChecker.check(diceValues);
            playerRollHandler.roll(request.game, request.player, diceValues, databaseCommunicator)
                .then((player) => {
                    const potentialScorecardWithYahtzeeExceptions = yahtzeeExceptionChecker.check(potentialScorecard, player.scorecard, diceValues);
                    response.status(200).send(
                        {
                            dice: diceValues,
                            scorecard: potentialScorecardWithYahtzeeExceptions,
                            player: player
                        }
                    );
                    next();
                })
                .catch((error) => {
                    response = response.status(500);
                    logger.error(error);
                    response.send({ error: error.message });
                });
        }

        return endpoint
    }
}