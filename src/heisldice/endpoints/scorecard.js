const blankScorecardHandler = require('../handlers/blank-scorecard-handler');

module.exports = {
    GET: (databaseCommunicator) => {
        return (request, response) => {
            const scorecard = blankScorecardHandler.getScorecard();
            response.status(200).send(scorecard);
        }
    }
}