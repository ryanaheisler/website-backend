const configuration = require('../../configuration.js');
const mongo = require('mongodb');

class DatabaseCommunicator {

  constructor() {
    const mongoClient = mongo.MongoClient(configuration.getMongoURI());
    this.clientPromise = mongoClient.connect().then((client) => {
      this.client = client;
      this.db = client.db(configuration.getMongoDBName());
    });
  }

  whenReady() {
    return new Promise((resolve, reject) => {
      this.clientPromise.then(() => {
        resolve(this);
      });
    });
  }

  // Yahztee Games
  insertGame(game) {
    const collection = this.db.collection(configuration.getMongoGamesCollection());
    return collection.insertOne(game);
  }

  getCurrentGame(gameID) {
    const collection = this.db.collection(configuration.getMongoGamesCollection());
    return collection.find({ gameID: gameID }).limit(1).next();
  }

  updateGame(updatedGame) {
    const collection = this.db.collection(configuration.getMongoGamesCollection());
    return collection.update({ _id: updatedGame._id }, updatedGame);
  }

  // Dominoes Lobbies
  insertDominoesLobby(lobby) {
    const collection = this.db.collection(configuration.getMongoDominoesLobbies());
    return collection.insertOne(lobby);
  }

  getCurrentDominoesLobby(lobbyID) {
    const collection = this.db.collection(configuration.getMongoDominoesLobbies());
    return collection.find({ lobbyID: lobbyID }).limit(1).next();
  }

  updateDominoesLobby(updatedLobby) {
    const collection = this.db.collection(configuration.getMongoDominoesLobbies());
    return collection.update({ _id: updatedLobby._id }, updatedLobby);
  }

  close() {
    this.client.close();
  }
}

module.exports = DatabaseCommunicator;