const express = require('express');
const lobbiesEndpoints = require('../dominoes/endpoints/lobbies');
const LobbyExtractor = require('../dominoes/middleware/lobby-extractor');

module.exports = {
    getRouter: (databaseCommunicator) => {
        const router = express.Router();

        router.post('/lobbies', lobbiesEndpoints.POST(databaseCommunicator));
        router.get('/lobbies/:lobbyID', LobbyExtractor(databaseCommunicator), lobbiesEndpoints.GET(databaseCommunicator));

        return router;
    }
}