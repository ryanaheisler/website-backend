const express = require('express');
const gamesEndpoints = require('../heisldice/endpoints/games');
const playersEndpoints = require('../heisldice/endpoints/players');
const diceEndpoints = require('../heisldice/endpoints/dice');
const scoreEndpoints = require('../heisldice/endpoints/score');
const diceColorEndpoints = require('../heisldice/endpoints/colors');
const scorecardsEndpoints = require('../heisldice/endpoints/scorecard');
const GameExtractor = require('../heisldice/middleware/game-extractor');
const PlayerBodyExtractor = require('../heisldice/middleware/player-body-extractor');
const PlayerParamsExtractor = require('../heisldice/middleware/player-param-extractor');
const BroadcastGame = require('../heisldice/middleware/game_broadcaster');
const IsItMyTurn = require('../heisldice/middleware/is-it-my-turn');

module.exports = {
    getRouter: (databaseCommunicator) => {
        const router = express.Router();

        const gameExtractor = GameExtractor(databaseCommunicator);
        const playerExtractorGetsNameFromBody = PlayerBodyExtractor();
        const playerExtractorGetsNameFromParams = PlayerParamsExtractor();

        const isItMyTurn = IsItMyTurn(databaseCommunicator);
        const gameBroadcaster = BroadcastGame(databaseCommunicator);

        router.post('/games',
            gamesEndpoints.POST(databaseCommunicator)
        );
        router.get('/games/:gameID',
            gameExtractor,
            gamesEndpoints.GET(databaseCommunicator)
        );
        router.put('/games/:gameID/players',
            gameExtractor,
            playerExtractorGetsNameFromBody,
            playersEndpoints.PUT(databaseCommunicator),
            gameBroadcaster
        );
        router.post('/games/:gameID/players/:name/dice',
            gameExtractor,
            playerExtractorGetsNameFromParams,
            isItMyTurn,
            diceEndpoints.POST(databaseCommunicator),
            gameBroadcaster
        );
        router.post('/games/:gameID/players/:name/score',
            gameExtractor,
            playerExtractorGetsNameFromParams,
            isItMyTurn,
            scoreEndpoints.POST(databaseCommunicator),
            gameBroadcaster
        );
        router.put('/games/:gameID/players/:name/dice-color/:diceColor',
            gameExtractor,
            playerExtractorGetsNameFromParams,
            diceColorEndpoints.PUT(databaseCommunicator),
            gameBroadcaster
        );
        router.get('/scorecard',
            scorecardsEndpoints.GET(databaseCommunicator)
        );

        return router;
    }
}