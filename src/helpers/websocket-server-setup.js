const url = require('url');
const SocketMap = require('./socket-map');
const setIntervalWrapper = require('./set-interval-wrapper');
const pruneAndPingWebsockets = require('./prune-websockets');

function setup() {
    setIntervalWrapper(pruneAndPingWebsockets, 15000);

    return (websocketServer) => {
        const socketMap = SocketMap.SINGLETON();
        websocketServer.on('connection', (websocket, request) => {
            console.log("Received connection request from", request.headers.origin);

            const queryParams = url.parse(request.url, true).query;
            socketMap.add(websocket, queryParams.gameID);

            websocket.isAlive = true;

            websocket.on('message', () => {
                websocket.close(4506, "No messages allowed");
            });

            websocket.on('close', (event) => {
                console.log('Connection closed:', JSON.stringify(event));
            });

            websocket.on('error', (event) => {
                console.log('Connection error:', JSON.stringify(event));
            });

            websocket.on('pong', () => {
                websocket.isAlive = true;
            });
        });
    };
}

module.exports = setup()