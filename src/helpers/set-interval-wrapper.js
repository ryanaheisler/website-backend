module.exports = (callback, delay) => {
    return setInterval(callback, delay);
}