module.exports = {
  getID() {
    let string = ""
    for (let index = 0; index < 4; index++) {
      string = `${string}${getRandomLetter()}`
    }
    return string;
  }
}

let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

function getRandomLetter() {

  const index = Math.floor(Math.random() * characters.length);

  return characters[index];
}