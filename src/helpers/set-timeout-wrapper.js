module.exports = (callback, timeoutInMilliseconds) => {
  setTimeout(callback, timeoutInMilliseconds);
}