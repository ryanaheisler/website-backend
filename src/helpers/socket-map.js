class SocketMap {

  static SINGLETON() {
    if (!SocketMap.instance) {
      SocketMap.instance = new SocketMap();
    }
    return SocketMap.instance;
  }

  constructor() {
    this.socketMap = {};
  }

  getClients(forID) {
    return this.socketMap[forID] || [];
  }

  getAllClients() {
    let allClients = [];

    Object.values(this.socketMap).forEach((array) => {
      allClients = allClients.concat(array);
    });

    return allClients;
  }

  add(socket, toID) {
    const clients = this.getClients(toID);
    clients.push(socket);
    this.socketMap[toID] = clients;
  }

  removeGroup(ID) {
    delete this.socketMap[ID];
  }

  removeWebsocket(websocketToRemove) {
    Object.keys(this.socketMap).forEach((key) => {
      this.socketMap[key] = this.getClients(key).filter((websocket) => {
        return !(websocket === websocketToRemove);
      });
    })
  }
}

module.exports = SocketMap;