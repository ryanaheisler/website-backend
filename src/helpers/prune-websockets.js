const SocketMap = require('./socket-map');

module.exports = () => {
    const socketMap = SocketMap.SINGLETON();
    const clients = socketMap.getAllClients();

    clients.forEach(client => {
        if (client.isAlive === false) {
            socketMap.removeWebsocket(client);
            return client.terminate();
        }

        client.isAlive = false;
        client.ping(noOp);
    });
}

function noOp() {
    return () => { };
}
