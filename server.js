const express = require('express');
const cors = require('cors');
const configuration = require('./configuration.js');
const bodyParser = require('body-parser')
const DatabaseCommunicator = require('./src/database-communicator/database-communicator')
const WebSocket = require('ws');
const http = require('http');
const setupWebsocketServer = require('./src/helpers/websocket-server-setup');
const heisldiceRoutes = require('./src/routers/heisldice-routes');
const dominoesRoutes = require('./src/routers/dominoes-routes');

const app = express();
const server = http.createServer(app);
const webSocketServer = new WebSocket.Server({ server });
setupWebsocketServer(webSocketServer);

var corsOptions = {
  origin: function (origin, callback) {
    if (configuration.getCorsWhitelist().indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error(`${origin} not allowed by CORS`))
    }
  }
}

app.use(cors(corsOptions));

app.use(bodyParser.json());

let databaseCommunicator;

new DatabaseCommunicator().whenReady().then((communicator) => {
  databaseCommunicator = communicator;

  const heisldiceRouter = heisldiceRoutes.getRouter(databaseCommunicator);
  app.use('/heisldice', heisldiceRouter);
  
  const dominoesRouter = dominoesRoutes.getRouter(databaseCommunicator);
  app.use('/dominoes', dominoesRouter);

  app.get('/*', function (request, response) {
    response.status(404).send();
  });

  server.listen(process.env.PORT || configuration.getPort());
});
